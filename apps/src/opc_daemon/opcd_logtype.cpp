#include "opcd_logtype.hpp"

namespace opcd
{
    const std::string LogType::Info = "INFO";
    const std::string LogType::Warn = "WARN";
    const std::string LogType::Debug = "DEBUG";
    const std::string LogType::Error = "ERROR";
    const std::string LogType::All = "ALL";
}