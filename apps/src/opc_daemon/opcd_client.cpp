#include "opcd_client.hpp"

namespace opcd
{
    
    Client::Client( OpcUa::UaClient * instance, SubscriptionClient * subscriptionClient, OpcUa::Subscription::SharedPtr subscription )
    {
        this->instance = instance;
        this->subscriptionClient = subscriptionClient;
        this->subscription = subscription;
    }

    Client::~Client()
    {
        subscription->Delete();

        delete subscriptionClient;
        delete instance;
    }
} // namespace opcd