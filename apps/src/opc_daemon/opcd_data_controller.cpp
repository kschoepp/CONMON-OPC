#include "opcd_data_controller.hpp"

namespace opcd
{
    DataController::DataController( Daemon * daemon )
    {
        setDaemonInstance( daemon );
        config = daemonInstance()->configInstance();
        logger = daemonInstance()->loggerInstance();
        databaseConnector = daemonInstance()->databaseConnectorInstance();
    }

    DataController::~DataController()
    {
        daemonInstance()->listenerInstance()->deleteClients();
    }

    void DataController::setDaemonInstance( Daemon * daemon )
    {
		if ( daemon == nullptr )
		{
			throw OPCDDaemonInstanceEmptyException( __FILE__, __LINE__ );
		}
		
		this->daemon = daemon;
    }

    Daemon * DataController::daemonInstance()
    {
		return daemon;
    }

    template <typename T>
    int DataController::addObject( T * object )
    {
        if ( object == nullptr )
        {
            throw std::invalid_argument( "Object passed has not been initialized. (nullptr)" );
        }
        
        int objectId = object->getID();
        std::map<int, T *> * objectContainer = getObjectsContainer<T>();
        (*objectContainer)[objectId] = object;
        
        logger->writeInfo( std::string( "" ) + typeid(*this).name() + ": " + typeid( T ).name() + "with ID '" + std::to_string( objectId ) + "' has been added to the pool." );
        
        return objectId;
    }

    template <typename T>
    T * DataController::getObject( int objectId )
    {
        std::map<int, T *> * objectContainer = getObjectsContainer<T>();

        return (*objectContainer)[objectId];
    }

    template <typename T>
    bool DataController::deleteObject( int objectId )
    {
        std::map<int, T *> * objectContainer = getObjectsContainer<T>();
        
        if ( getObject<T>( objectId ) != nullptr )
        {
            getObject<T>( objectId )->setActive( false ); // Workaround
            //delete getObject<T>( objectId ); // FIXME: Fix deleting without getting an Segmentation fault. It's only occuring, if the sensor is markes as active. (Data Races?)
            //objectContainer->erase( objectId ); // FIXME

            logger->writeInfo( std::string( "" ) + typeid(*this).name() + ": " + typeid( T ).name() + " with ID '" + std::to_string( objectId ) + "' has been deleted from the pool." );

            return true;
        }
        else
        {
            return false;
        }
    }

    template <typename T>
    std::vector<int> DataController::addObjects()
    {
        std::vector<int> objectsList;
        std::string databaseTableName;
        std::string databaseColumnName_ID;

        const std::type_info & type = typeid( T );

        if ( type == typeid( Machine ) )
        {
            databaseTableName = (*config)["DB_MACHINE_TABLE"];
            databaseColumnName_ID = (*config)["DB_MACHINE_COLUMN_ID"];
        }
        else if ( type == typeid( Sensor ) )
        {
            databaseTableName = (*config)["DB_SENSOR_TABLE"];
            databaseColumnName_ID = (*config)["DB_SENSOR_COLUMN_ID"];
        }
        else if ( type == typeid( DataTarget ) )
        {
            databaseTableName = (*config)["DB_DATATARGET_TABLE"];
            databaseColumnName_ID = (*config)["DB_DATATARGET_COLUMN_ID"];
        }
        else
        {
            throw std::invalid_argument( std::string( "" ) + "Only the classes " + typeid( Machine ).name() + ", " + typeid( Sensor ).name() + " and " + typeid( DataTarget ).name() + " are allowed as templating arguments." );
        }

        std::string select = "SELECT * FROM " + databaseTableName + ";";
        pqxx::result r = databaseConnector->query( select );

        std::map<int, T *> * objectContainer = getObjectsContainer<T>();

        for ( int rowNum = 0; rowNum < r.size(); ++rowNum )
        {
            const pqxx::row row = r[rowNum];
            int objectId = row[databaseColumnName_ID].as<int>();

            // If the Object doesn't exit yet, add it
            if ( getObject<T>( objectId ) == nullptr )
            {
                T * object;
                if ( type == typeid( Machine ) )
                {
                    Machine * machine = new Machine(    objectId,
                                                        row[(*config)["DB_MACHINE_COLUMN_URI"]].as<std::string>(),
                                                        row[(*config)["DB_MACHINE_COLUMN_NAME"]].as<std::string>(),
                                                        row[(*config)["DB_MACHINE_COLUMN_ACTIVE"]].as<bool>(),
                                                        row[(*config)["DB_MACHINE_COLUMN_CREATIONTIME"]].as<std::string>(),
                                                        row[(*config)["DB_MACHINE_COLUMN_LOCATION"]].as<std::string>(),
                                                        row[(*config)["DB_MACHINE_COLUMN_DESCRIPTION"]].as<std::string>() );
                    
                    object = reinterpret_cast<T *>( machine );
                }
                else if ( type == typeid( Sensor ) )
                {
                    Sensor * sensor = new Sensor(   objectId,
                                                    row[(*config)["DB_SENSOR_COLUMN_MACHINEID"]].as<int>(),
                                                    row[(*config)["DB_SENSOR_COLUMN_NAMESPACE"]].as<unsigned int>(),
                                                    row[(*config)["DB_SENSOR_COLUMN_NODEID"]].as<unsigned int>(),
                                                    row[(*config)["DB_SENSOR_COLUMN_DATATYPE"]].as<std::string>(),
                                                    row[(*config)["DB_SENSOR_COLUMN_UNIT"]].as<std::string>(),
                                                    row[(*config)["DB_SENSOR_COLUMN_MINDELTA"]].as<double>(),
                                                    row[(*config)["DB_SENSOR_COLUMN_NAME"]].as<std::string>(),
                                                    row[(*config)["DB_SENSOR_COLUMN_ACTIVE"]].as<bool>(),
                                                    row[(*config)["DB_SENSOR_COLUMN_CREATIONTIME"]].as<std::string>(),
                                                    row[(*config)["DB_SENSOR_COLUMN_DATATARGETS"]].as<std::string>() );

                    object = reinterpret_cast<T *>( sensor );
                }
                else if ( type == typeid( DataTarget ) )
                {
                    DataTarget * dataTarget = new DataTarget(   objectId,
                                                                row[(*config)["DB_DATATARGET_COLUMN_TYPE"]].as<std::string>(),
                                                                row[(*config)["DB_DATATARGET_COLUMN_DESTINATION"]].as<std::string>(),
                                                                row[(*config)["DB_DATATARGET_COLUMN_USERNAME"]].as<std::string>(),
                                                                row[(*config)["DB_DATATARGET_COLUMN_PASSWORD"]].as<std::string>(),
                                                                row[(*config)["DB_DATATARGET_COLUMN_CONNECTED"]].as<bool>(),
                                                                row[(*config)["DB_DATATARGET_COLUMN_DESCRIPTION"]].as<std::string>() );
                    
                    object = reinterpret_cast<T *>( dataTarget );
                }

                (*objectContainer)[objectId] = object;
                objectsList.push_back( objectId );
            }
        }

        logger->writeInfo( std::string( "" ) + typeid(*this).name() + ": " + std::to_string( objectsList.size() ) + " new " + type.name() + "s have been added to the pool." );

        return objectsList;
    }

    template <typename T>
    std::map<int, T *> * DataController::getObjectsContainer()
    {
        std::map<int, T *> * objectContainer;

        const std::type_info & type = typeid( T );
        
        if ( type == typeid( Machine ) )
        {
            objectContainer = reinterpret_cast<std::map<int, T *> *>( &machines );
        }
        else if ( type == typeid( Sensor ) )
        {
            objectContainer = reinterpret_cast<std::map<int, T *> *>( &sensors );
        }
        else if ( type == typeid( DataTarget ) )
        {
            objectContainer = reinterpret_cast<std::map<int, T *> *>( &dataTargets );
        }
        else
        {
            throw std::invalid_argument( std::string( "" ) + "Only the classes " + typeid( Machine ).name() + ", " + typeid( Sensor ).name() + " and " + typeid( DataTarget ).name() + " are allowed as templating arguments." );
        }

        return objectContainer;
    }

    template <typename T>
    std::vector<int> DataController::deleteObjects()
    {
        std::vector<int> objectsList;
        std::string databaseTableName;
        std::string databaseColumnName_ID;

        const std::type_info & type = typeid( T );

        if ( type == typeid( Machine ) )
        {
            databaseTableName = (*config)["DB_MACHINE_TABLE"];
            databaseColumnName_ID = (*config)["DB_MACHINE_COLUMN_ID"];
        }
        else if ( type == typeid( Sensor ) )
        {
            databaseTableName = (*config)["DB_SENSOR_TABLE"];
            databaseColumnName_ID = (*config)["DB_SENSOR_COLUMN_ID"];
        }
        else if ( type == typeid( DataTarget ) )
        {
            databaseTableName = (*config)["DB_DATATARGET_TABLE"];
            databaseColumnName_ID = (*config)["DB_DATATARGET_COLUMN_ID"];
        }
        else
        {
            throw std::invalid_argument( std::string( "" ) + "Only the classes " + typeid( Machine ).name() + ", " + typeid( Sensor ).name() + " and " + typeid( DataTarget ).name() + " are allowed as templating arguments." );
        }

        typename std::map<int, T *> * objectContainer = getObjectsContainer<T>();

        // Remove deleted Objects
        for ( typename std::map<int, T *>::iterator it = objectContainer->begin(); it != objectContainer->end(); ++it )
        {
            int objectId = it->first;

            std::string select = "SELECT " + databaseColumnName_ID + " FROM " + databaseTableName + " WHERE " + databaseColumnName_ID + " = " + std::to_string( objectId ) + ";";
            pqxx::result r = databaseConnector->query( select );

            // This needs to be saved, because the current entry is being deleted, and thus it will point to null.
            typename std::map<int, T *>::iterator next = std::next( it, 1 );
            
            // If a Object has been deleted from the DB, delete & append negated ID to list.
            if ( r.size() == 0 )
            {
                deleteObject<T>( objectId );
                it = next;

                objectsList.push_back( objectId * -1 );
            }
        }

        logger->writeInfo( std::string( "" ) + typeid(*this).name() + ": " + std::to_string( objectsList.size() ) + " " + type.name() + "s have been deleted from the pool." );

        return objectsList;
    }

    template <typename T>
    std::vector<int> DataController::updateObjects()
    {
        std::vector<int> objectsList;
        std::string databaseTableName;
        std::string databaseColumnName_ID;
        std::string databaseColumnName_Changed;

        const std::type_info & type = typeid( T );

        if ( type == typeid( Machine ) )
        {
            databaseTableName = (*config)["DB_MACHINE_TABLE"];
            databaseColumnName_ID = (*config)["DB_MACHINE_COLUMN_ID"];
            databaseColumnName_Changed = (*config)["DB_MACHINE_COLUMN_CHANGED"];
        }
        else if ( type == typeid( Sensor ) )
        {
            databaseTableName = (*config)["DB_SENSOR_TABLE"];
            databaseColumnName_ID = (*config)["DB_SENSOR_COLUMN_ID"];
            databaseColumnName_Changed = (*config)["DB_SENSOR_COLUMN_CHANGED"];
        }
        else if ( type == typeid( DataTarget ) )
        {
            databaseTableName = (*config)["DB_DATATARGET_TABLE"];
            databaseColumnName_ID = (*config)["DB_DATATARGET_COLUMN_ID"];
            databaseColumnName_Changed = (*config)["DB_DATATARGET_COLUMN_CHANGED"];
        }
        else
        {
            throw std::invalid_argument( std::string( "" ) + "Only the classes " + typeid( Machine ).name() + ", " + typeid( Sensor ).name() + " and " + typeid( DataTarget ).name() + " are allowed as templating arguments." );
        }
        
        // Remove deleted Objects
        std::vector<int> temp = deleteObjects<T>();
        objectsList.insert( objectsList.end(), temp.begin(), temp.end() );
        
        // Add new Objects
        temp = addObjects<T>();
        objectsList.insert( objectsList.end(), temp.begin(), temp.end() );
        
        // Update changed Objects
        std::string select = "SELECT * FROM " + databaseTableName + ";";
        pqxx::result r = databaseConnector->query( select );

        for ( int rowNum = 0; rowNum < r.size(); ++rowNum )
        {
            const pqxx::row row = r[rowNum];

            int objectId = row[databaseColumnName_ID].as<int>();
            bool objectChanged = row[databaseColumnName_Changed].as<bool>();

            if ( objectChanged )
            {
                T * object;

                if ( type == typeid( Machine ) )
                {
                    Machine * machine = getMachine( objectId );
                        machine->setName( row[(*config)["DB_MACHINE_COLUMN_NAME"]].as<std::string>() );
                        machine->setActive( row[(*config)["DB_MACHINE_COLUMN_ACTIVE"]].as<bool>() );
                        machine->setLocation( row[(*config)["DB_MACHINE_COLUMN_LOCATION"]].as<std::string>() );
                        machine->setDescription( row[(*config)["DB_MACHINE_COLUMN_DESCRIPTION"]].as<std::string>() );

                    object = reinterpret_cast<T *>( machine );
                }
                else if ( type == typeid( Sensor ) )
                {
                    Sensor * sensor = getSensor( objectId );
                        sensor->setUnit( row[(*config)["DB_SENSOR_COLUMN_UNIT"]].as<std::string>() );
                        sensor->setMinDelta( row[(*config)["DB_SENSOR_COLUMN_MINDELTA"]].as<double>() );
                        sensor->setName( row[(*config)["DB_SENSOR_COLUMN_NAME"]].as<std::string>() ); 
                        sensor->setActive( row[(*config)["DB_SENSOR_COLUMN_ACTIVE"]].as<bool>() );
                        sensor->setDataTargets( row[(*config)["DB_SENSOR_COLUMN_DATATARGETS"]].as<std::string>() );

                    object = reinterpret_cast<T *>( sensor );
                }
                else if ( type == typeid( DataTarget ) )
                {
                    DataTarget * dataTarget = getDataTarget( objectId );
                    dataTarget->setConnected( row[(*config)["DB_DATATARGET_COLUMN_CONNECTED"]].as<bool>() );

                    object = reinterpret_cast<T *>( dataTarget );
                }

                // Set "changed" for the corresponding Object to false in the Database
                std::string update =    "UPDATE " + databaseTableName +
                                        " SET " + databaseColumnName_Changed + " = FALSE"
                                        " WHERE " + databaseColumnName_ID + "= " + std::to_string( objectId ) + ";"; 
                databaseConnector->query( update );

                objectsList.push_back( objectId );
            }
        }

        logger->writeInfo( std::string( "" ) + typeid(*this).name() + ": " + std::to_string( objectsList.size() ) + " " + type.name() + "s have been updated." );

        return objectsList;
    }

    int DataController::addMachine( Machine * machine )
    {
        return addObject<Machine>( machine );
    }

    Machine * DataController::getMachine( int machineId )
    {
        return getObject<Machine>( machineId );
    }

    Machine * DataController::getMachineBySensor( int sensorId )
    {
        if ( sensors.find( sensorId ) != sensors.end() && sensors[sensorId] != nullptr )
        {
            int machineId = sensors[sensorId]->getMachineID();

            return getMachine( machineId );
        }

        return nullptr;
    }

    bool DataController::deleteMachine( int machineId )
    {
        return deleteObject<Machine>( machineId );
    }

    int DataController::addSensor( Sensor * sensor )
    {
        return addObject<Sensor>( sensor );
    }

    Sensor * DataController::getSensor( int sensorId )
    {
        return getObject<Sensor>( sensorId );
    }

    Sensor * DataController::getSensor( int machineId, unsigned int nodeNamespace, unsigned int nodeId )
    {
        Sensor * sensor = nullptr;

        for ( typename std::map<int, Sensor *>::iterator it = getSensorsContainer()->begin(); it != getSensorsContainer()->end(); ++it )
        {
            Sensor * curSensor = it->second;

            if ( curSensor->getMachineID() == machineId && curSensor->getNamespace() == nodeNamespace && curSensor->getNodeID() == nodeId )
            {
                sensor = curSensor;

                break;
            }
        }

        return sensor;
    }

    bool DataController::deleteSensor( int sensorId )
    {
        return deleteObject<Sensor>( sensorId );
    }

    int DataController::addDataTarget( DataTarget * dataTarget )
    {
        return addObject<DataTarget>( dataTarget );
    }

    DataTarget * DataController::getDataTarget( int dataTargetId )
    {
        return dataTargets[dataTargetId];
    }

    bool DataController::deleteDataTarget( int dataTargetId )
    {
        return deleteObject<DataTarget>( dataTargetId );
    }

    std::vector<int> DataController::addMachines()
    {
        return addObjects<Machine>();
    }

    std::map<int, Machine *> * DataController::getMachinesContainer()
    {
        return getObjectsContainer<Machine>();
    }

    int DataController::getNumMachines()
    {
        return getMachinesContainer()->size();
    }

    std::vector<int> DataController::deleteMachines()
    {
        return deleteObjects<Machine>();
    }

    std::vector<int> DataController::updateMachines()
    {
        return updateObjects<Machine>();
    }

    std::vector<int> DataController::addSensors()
    {
        return addObjects<Sensor>();
    }

    std::map<int, Sensor *> * DataController::getSensorsContainer()
    {
        return getObjectsContainer<Sensor>();
    }

    std::map<int, Sensor *> DataController::getSensorsContainer( int machineId )
    {
        std::map<int, Sensor *> sensorsByMachine;

        for ( std::map<int, Sensor *>::iterator it = getSensorsContainer()->begin(); it != getSensorsContainer()->end(); ++it )
        {
            int sensorId = it->first;
            Sensor * sensor = it->second;
            
            if ( sensor->getMachineID() == machineId )
            {
                sensorsByMachine[it->first] = sensor;
            }
        }

        return sensorsByMachine;
    }

    int DataController::getNumSensors()
    {
        return getSensorsContainer()->size();
    }

    std::vector<int> DataController::deleteSensors()
    {
        return deleteObjects<Sensor>();
    }

    std::vector<int> DataController::updateSensors()
    {
        return updateObjects<Sensor>();
    }

    std::vector<int> DataController::addDataTargets()
    {
        return addObjects<DataTarget>();
    }

    std::map<int, DataTarget *> * DataController::getDataTargetsContainer()
    {
        return getObjectsContainer<DataTarget>();;
    }

    int DataController::getNumDataTargets()
    {
        return getDataTargetsContainer()->size();
    }
    
    std::vector<int> DataController::deleteDataTargets()
    {
        return deleteObjects<DataTarget>();
    }

    std::vector<int> DataController::updateDataTargets()
    {
        return updateObjects<DataTarget>();
    }
    
    void DataController::addData()
    {
        addMachines();
        addSensors();
        addDataTargets();
    }

    void DataController::updateData()
    {
        updateMachines();
        updateSensors();
        updateDataTargets();
    }

    void DataController::deleteData()
    {
        deleteMachines();
        deleteSensors();
        deleteDataTargets();
    }
} // namespace opcd