#include "opcd_database_connector.hpp"

namespace opcd
{
    DatabaseConnector::DatabaseConnector( std::string host, std::string port, std::string databaseName, std::string user, std::string password )
    {
        setHost( host );
        setPort( port );
        setDatabaseName( databaseName );
        setUser( user );
        setPassword( password );
        updateConnectionString();
    }

    DatabaseConnector::~DatabaseConnector()
    {
        if ( databaseConnection != nullptr )
        {
            delete databaseConnection;
        }
    }

    void DatabaseConnector::DatabaseConnector::setHost( std::string host )
    {
        this->host = host;
    }

    std::string DatabaseConnector::getHost()
    {
        return host;
    }

    void DatabaseConnector::setPort( std::string port )
    {
        this->port = port;
    }

    std::string DatabaseConnector::getPort()
    {
        return port;
    }

    void DatabaseConnector::setDatabaseName( std::string databaseName )
    {
        this->databaseName = databaseName;
    }

    std::string DatabaseConnector::getDatabaseName()
    {
        return databaseName;
    }

    void DatabaseConnector::setUser( std::string user )
    {
        this->user = user;
    }

    std::string DatabaseConnector::getUser()
    {
        return user;
    }

    void DatabaseConnector::setPassword( std::string password )
    {
        this->password = password;
    }

    std::string DatabaseConnector::getPassword()
    {
        return password;
    }

    void DatabaseConnector::setConnectionString( std::string connectionString )
    {
        this->connectionString = connectionString;
    }

    std::string DatabaseConnector::getConnectionString()
    {
        return connectionString;
    }

    std::string DatabaseConnector::generateConnectionString()
    {
        return "host = " + getHost() + " port = " + getPort() + " dbname = " + getDatabaseName() + " user = " + getUser() + " password = " + getPassword();
    }

    void DatabaseConnector::updateConnectionString()
    {
        setConnectionString( generateConnectionString() );
    }

    void DatabaseConnector::connect()
    {
        if ( isConnected() )
        {
            disconnect();
        }

        databaseConnection = new pqxx::connection( getConnectionString() );
    }

    bool DatabaseConnector::isConnected()
    {
        if ( databaseConnection != nullptr && databaseConnection->is_open() )
        {
            return true;
        }

        return false;
    }

    void DatabaseConnector::prepare( std::string name, std::string statement )
    {
        checkDatabaseConnection();

        databaseConnection->prepare( name, statement );
    }

    pqxx::result DatabaseConnector::queryPrepared( std::string name )
    {
        checkDatabaseConnection();

        pqxx::work transaction( *databaseConnection );
        pqxx::result res = transaction.prepared( name ).exec();
        transaction.commit();

        return res;
    }

    pqxx::result DatabaseConnector::query( std::string statement )
    {
        checkDatabaseConnection();

        pqxx::work transaction( *databaseConnection );
        pqxx::result res = transaction.exec( statement );
        transaction.commit();

        return res;
    }

    void DatabaseConnector::disconnect()
    {
        if ( isConnected() )
        {
            databaseConnection->disconnect();
        }
    }

    void DatabaseConnector::checkDatabaseConnection()
    {
        if ( !isConnected() )
        {
            throw OPCDDatabaseNotConnectedException();
        }
    }
} // namespace opcd