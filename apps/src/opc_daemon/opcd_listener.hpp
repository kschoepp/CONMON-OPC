#ifndef OPCD_LISTENER_HPP
#define OPCD_LISTENER_HPP

// Standard Libraries
#include <string>
#include <vector>
#include <map>
#include <thread>
#include <exception>

// OpcUa
#include <opc/ua/client/client.h>
#include <opc/ua/protocol/nodeid.h>
#include <opc/ua/node.h>
#include <opc/common/logger.h>

// opcd
#include "opcd_daemon.hpp"
#include "opcd_data_controller.hpp"
#include "opcd_client.hpp"
#include "opcd_exceptions.hpp"
#include "opcd_logger.hpp"
#include "opcd_machine.hpp"
#include "opcd_sensor.hpp"
#include "opcd_subscription_client.hpp"

namespace opcd
{
    class Daemon;
    class DataController;
    class SubscriptionClient;
    class Client;

    class Listener
    {        
    private:
        Daemon * daemon;
        Logger * logger;
        DataController * dataController;
        
        unsigned int clock;

        std::map<int, Client *> clients; // machineID, Client
        std::map<int, unsigned int> handles; // sensorID, handle

        void setDaemonInstance( Daemon * daemon );
        void setClock( unsigned int clock );

        Client * addClient( int machineId );
        Client * getClient( int machineId );
        bool deleteClient( int machineId );

        unsigned int addSubscription( int sensorId );
        unsigned int getSubscriptionHandle( int sensorId );
        bool deleteSubscription( int sensorId );
        
    public:
        Listener( Daemon * daemon, int clock );
        ~Listener();
        
        Daemon * daemonInstance();
        unsigned int getClock();

        std::vector<int> addClients();
        std::vector<int> deleteClients();
        std::vector<int> updateClients();
        
        std::vector<int> addSubscriptions( int machineId );
        std::vector<int> deleteSubscriptions( int machineId );
        std::vector<int> updateSubscriptions( int machineId );
    };
} // namespace opcd

#endif // OPCD_LISTENER_HPP