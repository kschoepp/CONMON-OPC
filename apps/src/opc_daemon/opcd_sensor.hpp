#ifndef OPCD_SENSOR_HPP
#define OPCD_SENSOR_HPP

// Standard Libraries
#include <string>

// opcd
#include "opcd_exceptions.hpp"

namespace opcd
{
    class Sensor
    {
    private:
        int id;
        int machineId;
        unsigned int nodeNamespace;
        unsigned int nodeId;
        std::string dataType;
        std::string unit;
        double minDelta;
        std::string name;
        bool active;
        std::string creationTime;
        std::string dataTargets;

        void setID( int id );
        void setMachineID( int id );
        void setNamespace( unsigned int nodeNamespace );
        void setNodeID( unsigned int nodeId );
        void setDataType( std::string dataType );
        void setCreationTime( std::string creationTime );
        
    public:
        Sensor( int id,
                int machineId,
                unsigned int nodeNamespace,
                unsigned int nodeId,
                std::string dataType,
                std::string unit,
                double minDelta,
                std::string name,
                bool active,
                std::string creationTime,
                std::string dataTargets
        );
        
        int getID();
        int getMachineID();
        unsigned int getNamespace();
        unsigned int getNodeID();
        std::string getDataType();
        void setUnit( std::string unit );
        std::string getUnit();
        void setMinDelta( double minDelta );
        double getMinDelta();
        void setName( std::string name );
        std::string getName();
        void setActive( bool active );
        bool isActive();
        std::string getCreationTime();
        void setDataTargets( std::string dataTargets );
        std::string getDataTargets();        
    };
} // namespace opcd

#endif // OPCD_SENSOR_HPP