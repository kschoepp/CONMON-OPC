#ifndef OPCD_DATABASE_CONNECTOR_HPP
#define OPCD_DATABASE_CONNECTOR_HPP

// Standard Libraries
#include <string>

// PostgreSQL Libs
#include <pqxx/pqxx>

// opcd
#include "opcd_exceptions.hpp" // opcd::ConfigMalformedException

namespace opcd
{
    class DatabaseConnector
    {
    private:
        pqxx::connection* databaseConnection = nullptr;
        std::string host;
        std::string port;
        std::string databaseName;
        std::string user;
        std::string password;
        std::string connectionString;
        
    public:
        DatabaseConnector( std::string host, std::string port, std::string databaseName, std::string user, std::string password );
        ~DatabaseConnector();
        
        void setHost( std::string host );
        std::string getHost();
        void setPort( std::string port );
        std::string getPort();
        void setDatabaseName( std::string databaseName );
        std::string getDatabaseName();
        void setUser( std::string user );
        std::string getUser();
        void setPassword( std::string password );
        std::string getPassword();
        void setConnectionString( std::string connectionString );
        std::string getConnectionString();
        std::string generateConnectionString();
        void updateConnectionString();
        void connect();
        bool isConnected();
        void prepare( std::string name, std::string statement );
        pqxx::result queryPrepared( std::string name );
        pqxx::result query( std::string statement );
        void disconnect();
        void checkDatabaseConnection();
    };
} // namespace opcd


#endif // OPCD_DATABASE_CONNECTOR_HPP