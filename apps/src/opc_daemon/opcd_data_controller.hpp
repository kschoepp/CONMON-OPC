#ifndef OPCD_DATA_CONTROLLER_HPP
#define OPCD_DATA_CONTROLLER_HPP

// Standard Libraries
#include <string>
#include <typeinfo>
#include <stdexcept>
#include <iostream>

// PostgreSQL Libs
#include <pqxx/pqxx>

// opcd
#include "opcd_config.hpp"
#include "opcd_data_target.hpp"
#include "opcd_database_connector.hpp"
#include "opcd_exceptions.hpp"
#include "opcd_listener.hpp"
#include "opcd_logger.hpp"
#include "opcd_machine.hpp"
#include "opcd_sensor.hpp"

namespace opcd
{
    class Daemon;
    class Config;
    class Logger;
    class DatabaseConnector;
    class Listener;

    class DataController
    {
    private:
        Daemon * daemon;
        Config * config;
        Logger * logger;
        DatabaseConnector * databaseConnector;
        Listener * listener;

        // Data Containers
        std::map<int, Machine *> machines; // machineID, Machine
        std::map<int, Sensor *> sensors; // sensorID, Sensor
        std::map<int, DataTarget * > dataTargets; // dataTargetID, DataTarget
        
        void setDaemonInstance( Daemon * daemon );

    public:
        DataController( Daemon * daemon );
        ~DataController();

        Daemon * daemonInstance();
        
        template <typename T> int addObject( T * data );
        template <typename T> T * getObject( int objectId );
        template <typename T> bool deleteObject( int dataId );

        template <typename T> std::vector<int> addObjects();
        template <typename T> std::map<int, T *> * getObjectsContainer();
        template <typename T> std::vector<int> deleteObjects();
        template <typename T> std::vector<int> updateObjects();

        int addMachine( Machine * machine );
        Machine * getMachine( int machineId );
        Machine * getMachineBySensor( int sensorId );
        bool deleteMachine( int machineId );

        int addSensor( Sensor * sensor );
        Sensor * getSensor( int sensorId );
        Sensor * getSensor( int machineId, unsigned int nodeNamespace, unsigned int nodeId );
        bool deleteSensor( int sensorId );

        int addDataTarget( DataTarget * dataTarget );
        DataTarget * getDataTarget( int dataTargetId );
        bool deleteDataTarget( int dataTargetId );

        std::vector<int> addMachines();
        std::map<int, Machine *> * getMachinesContainer();
        int getNumMachines();
        std::vector<int> deleteMachines();
        std::vector<int> updateMachines();

        std::vector<int> addSensors();
        std::map<int, Sensor *> * getSensorsContainer();
        std::map<int, Sensor *> getSensorsContainer( int machineId );
        int getNumSensors();
        std::vector<int> deleteSensors();
        std::vector<int> updateSensors();

        std::vector<int> addDataTargets();
        std::map<int, DataTarget *> * getDataTargetsContainer();
        int getNumDataTargets();
        std::vector<int> deleteDataTargets();
        std::vector<int> updateDataTargets();

        void addData();
        void updateData();
        void deleteData();
    };
} // namespace opcd

#endif // OPCD_DATA_CONTROLLER_HPP