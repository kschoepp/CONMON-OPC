#include <iostream>
#include <stdexcept>
#include <thread>

#include "opcd_daemon.hpp"

int main( int argc, char* argv[] )
{
    try
    {
        opcd::Daemon daemon( argv[1] );
        
		// Infinite Loop
		std::cout << "Ctrl-C to exit" << std::endl;
        for (;;)
        {
            std::cout << "--------------------" << std::endl;
    
            std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
        }
    }
    catch( pqxx::sql_error &e )
    {
        std::cerr << "Exception: " << "SQL error: " << e.what() << std::endl;
        std::cerr << "Exception: " << "Query was: " << e.query() << std::endl;

        return 3;
    }
    catch ( opcd::OPCDConfigMalformedException & e )
    {
        std::cerr << "OPCDConfigMalformedException: " << e.what() << std::endl;
        return 3;
    }
    catch ( opcd::OPCDConfigMissingException & e )
    {
        std::cerr << "OPCDConfigMissingException: " << e.what() << std::endl;

        return 3;
    }
    catch ( opcd::OPCDConfigKeyNotFoundException & e )
    {
        std::cerr << "OPCDConfigKeyNotFoundException: " << e.what() << std::endl;

        return 3;
    }
    catch ( opcd::OPCDDatabaseNotConnectedException & e )
    {
        std::cerr << "OPCDDatabaseNotConnectedException: " << e.what() << std::endl;

        return 3;
    }
    catch ( opcd::OPCDEmptyFieldException & e )
    {
        std::cerr << "OPCDEmptyFieldException: " << e.what() << std::endl;

        return 3;
    }
    catch ( opcd::OPCDDaemonInstanceEmptyException & e )
    {
        std::cerr << "OPCDDaemonInstanceEmptyException: " << e.what() << std::endl;

        return 3;
    }
    catch ( opcd::OPCDDataTargetTypeEmptyException & e )
    {
        std::cerr << "OPCDDataTargetTypeEmptyException: " << e.what() << std::endl;

        return 3;
    }
    catch ( opcd::OPCDClockTooLowException & e )
    {
        std::cerr << "OPCDClockTooLowException: " << e.what() << std::endl;

        return 3;
    }
    catch ( opcd::OPCDException & e )
    {
        std::cerr << "OPCDException: " << e.what() << std::endl;

        return 3;
    }
    catch( std::exception &e )
    {
        std::cerr << "exception: " << e.what() << std::endl;
        
        return 1;
    }
    
    return 0;
}