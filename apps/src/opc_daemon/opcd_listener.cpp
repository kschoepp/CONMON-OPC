#include "opcd_listener.hpp"

namespace opcd
{
    Listener::Listener( Daemon * daemon, int clock )
    {
        setDaemonInstance( daemon );
        logger = daemonInstance()->loggerInstance();
        dataController = daemonInstance()->dataControllerInstance();
        setClock( clock );
    }

    Listener::~Listener()
    {
        deleteClients();
    }

    void Listener::setDaemonInstance( Daemon * daemon )
    {
		if ( daemon == nullptr )
		{
			throw OPCDDaemonInstanceEmptyException( __FILE__, __LINE__ );
		}
		
		this->daemon = daemon;
    }

    Daemon * Listener::daemonInstance()
    {
		return daemon;
    }

    void Listener::setClock( unsigned int clock )
    {
        if ( !( clock > 0 ) )
        {
            throw OPCDClockTooLowException( __FILE__, __LINE__ );
        }

        this->clock = clock;
    }

    unsigned int Listener::getClock()
    {
        return clock;
    }

    Client * Listener::addClient( int machineId )
    {
        Machine * machine = dataController->getMachine( machineId );

        // If the machine doesn't exist, return nullptr.
        if ( machine == nullptr )
        {
            logger->writeError( std::string( "" ) + typeid(*this).name() + ": Machine with ID '" + std::to_string( machineId ) + "' not found." );

            return nullptr;
        }
        
        // If the machine is active, add a client and return it. Otherwise, return a nullptr.
        if ( machine->isActive() )
        {
            std::string protocol = "ocp.tcp";
            std::string endpoint = dataController->getMachine( machineId )->getURI();

            try
            {
                // Create a Logger instance for each UaClient
                // Note: We actually don't need this, but it unfortunately needs to be initialized for multiple clients to work correctly.
                std::string spd_loggerInstanceName = "opcd_daemon_client_" + std::to_string( machineId ); 
                std::shared_ptr<spdlog::logger> spd_logger = spdlog::get( spd_loggerInstanceName );

                // If the Logger doesn't exist yet, create one. This needs to be checked, so we can respawn a client correctly, in case it's been removed earlier.
                // If we don't check and just try to add one straightforward, spdlog::Logger will throw an Exception
                if ( spd_logger == nullptr )
                {
                    spd_logger = spdlog::stderr_color_mt( "opcd_daemon_client_" + std::to_string( machineId ) );
                    spd_logger->should_log( spdlog::level::level_enum::off );
                }

                // Initialize UaClient and add a SubscriptionClient for the current Machine
                OpcUa::UaClient * instance = new OpcUa::UaClient( spd_logger );
                    instance->Connect( protocol + "://" + endpoint + "/" );
                SubscriptionClient * subscriptionClient = new SubscriptionClient( daemonInstance(), machineId );
                OpcUa::Subscription::SharedPtr subscription = instance->CreateSubscription( getClock(), *subscriptionClient );
    
                // Add this Client with all needed data to our clients-Map
                clients[machineId] = new Client( instance, subscriptionClient, subscription );
    
                logger->writeInfo( std::string( "" ) + typeid(*this).name() + ": Added Client for Machine with ID '" + std::to_string( machineId ) + "'." );
                
                // Add all Subscriptions for this Machine
                addSubscriptions( machineId );
    
                return getClient( machineId );
            }
            catch ( std::exception& e )
            {
                logger->writeError( std::string( "" ) + typeid(*this).name() + ": Couldn't add Client for Machine with ID '" + std::to_string( machineId ) + "'. Exception thrown: '" + e.what() + "'." );

                return nullptr;
            }
        }
        else
        {
            logger->writeInfo( std::string( "" ) + typeid(*this).name() + ": Machine with ID '" + std::to_string( machineId ) + "' is not active; did not add a new Client." );

            return nullptr;
        }
    }

    Client * Listener::getClient( int machineId )
    {
        if ( clients.find( machineId ) != clients.end() )
        {
            return clients[machineId];
        }
        else
        {
            Machine * machine = dataController->getMachine( machineId );

            if ( machine != nullptr && machine->isActive() )
            {
                logger->writeWarn( std::string( "" ) + typeid(*this).name() + ": Couldn't find Client for Machine with ID '" + std::to_string( machineId ) + "'." );
            }

            return nullptr;
        }
    }

    bool Listener::deleteClient( int machineId )
    {
        Client * client = getClient( machineId );

        // If client exits, delete it and return true.
        if ( client != nullptr )
        {
            deleteSubscriptions( machineId );

            delete client;
            clients.erase( machineId );

            logger->writeInfo( std::string( "" ) + typeid(*this).name() + ": Deleted Client for Machine with ID '" + std::to_string( machineId ) + "'." );

            return true;
        }
        else
        {
            logger->writeWarn( std::string( "" ) + typeid(*this).name() + ": Couldn't find Client for Machine with ID '" + std::to_string( machineId ) + "'; nothing to delete." );

            return false;
        }
    }

    unsigned int Listener::addSubscription( int sensorId )
    {
        // Get sensorId.
        Sensor * sensor = dataController->getSensor( sensorId );
        
        // If the sensor doesn't exist, abort and return 0.
        if ( sensor == nullptr )
        {
            logger->writeError( std::string( "" ) + typeid(*this).name() + ": Sensor with ID '" + std::to_string( sensorId ) + "' not found." );

            return 0;
        }
         
        // Get machine.
        Machine * machine = dataController->getMachineBySensor( sensorId );
        
        // If the machine doesn't exist, abort and return 0.
        if ( machine == nullptr )
        {
            logger->writeError( std::string( "" ) + typeid(*this).name() + ": Machine for Sensor with ID'" + std::to_string( sensorId ) + "' not found. Could not add Sensor." );

            return 0;
        }

        int machineId = machine->getID();
        
        // Get client.
        Client * client = getClient( machineId );

        // If the corresponding client doesn't exist yet, try to add one.
        if ( client == nullptr )
        {
            client = addClient( machineId );

            // If the client couldn't be added either way, abort and return 0.
            if ( client == nullptr )
            {
                logger->writeWarn( std::string( "" ) + typeid(*this).name() + ": Couldn't add Client for Machine with ID '" + std::to_string( machineId ) + "'; Can't add Sensor with ID'" + std::to_string( sensorId ) + "'." );

                return 0;
            }
        }
        
        // If the sensor is active, add a subscription and return its' Handle. Otherwise, return a 0.
        // Also, check for nullptr again, in case one of the upper steps took a bit longer, and an deleteSensor event happened in the DataController.
        if ( sensor != nullptr && sensor->isActive() )
        {
            // Get node to particular sensorId
            OpcUa::Subscription::SharedPtr subscription = client->subscription;
            OpcUa::NodeId nodeId( sensor->getNodeID(), sensor->getNamespace() );
            OpcUa::Node node = client->instance->GetNode( nodeId ); // Get Node from Client
            
            ////// Checking for invalid Nodes was later fixed by me and a colleague; we've introduced a new Method for this in OpcUa::Node.
            /*
            OpcUa::Node parentNode = node.GetParent(); // Get parent of this node.
            
            // Check whether the node exists on the machine, and only try to subscribe to this node, if it exists.
            // If we're ignoring that and we'll try to subscribe anyways, the connected UaClient will throw an exception and crash.
            // HACK:    Find another way to check if the given node exists on the Server;
            //          - Since the Client will return a "valid" node with GetNode( nodeId ) even if it doesn't exist on the Serverside (Will give back the node passed to the function -> shitty implementation), we need to check if it has a (valid) parent.
            //          - If the Node doesn't exist on the Client, no parent can be found, and a Node(0,0) -with Namespace = 0 and NodeId = 0- will be returned.
            //          - If we now check for that condition, we can be pretty sure that the given node does or does not exist on the server, since the specification of OPCUA didn't intend to have a Node(0,0) in the Data-Model.
            if ( !( parentNode.GetId().GetNamespaceIndex() == 0 && parentNode.GetId().GetIntegerIdentifier() == 0 ) )
            */

            // Check whether the node exists on the machine, and only try to subscribe to this node, if it exists.
            // If we're ignoring that and we'll try to subscribe anyways, the connected UaClient will throw an exception and crash.
            if ( node.IsValid() )
            {
                try
                {
                    // Subscribe to DataChange events for the particular node
                    unsigned int handle = subscription->SubscribeDataChange( node );
                    handles[sensorId] = handle;
                    
                    logger->writeInfo( std::string( "" ) + typeid(*this).name() + ": Added Subscription for Sensor with ID '" + std::to_string( sensorId ) + "'; Handle: '" + std::to_string( handle ) + "'." );
    
                    return handle;
                }
                catch ( std::exception& e )
                {
                    logger->writeError( std::string( "" ) + typeid(*this).name() + ": Couldn't add Subscription for Sensor with ID '" + std::to_string( sensorId ) + "'. Exception thrown: '" + e.what() + "'." );
                    logger->writeWarn( std::string( "" ) + typeid(*this).name() + ": Client for Machine with ID '" + std::to_string( machineId ) + "' crashed. Please fix the Data for Sensor with ID '" + std::to_string( sensorId ) + "'." );

                    return 0;
                }
            }
            else
            {
                logger->writeWarn( std::string( "" ) + typeid(*this).name() + ": Couldn't subscribe to Sensor with ID '" + std::to_string( sensorId ) + "'; " + node.ToString() + " does not exist on the given machine." );
            
                return 0;
            }
        }
        else
        {
            logger->writeInfo( std::string( "" ) + typeid(*this).name() + ": Sensor with ID '" + std::to_string( sensorId ) + "' is not active; did not add a new Subscription." );

            return 0;
        }
    }

    unsigned int Listener::getSubscriptionHandle( int sensorId )
    {
        if ( handles.find( sensorId ) != handles.end() )
        {
            return handles[sensorId];
        }
        else
        {
            return 0;
        }
    }

    bool Listener::deleteSubscription( int sensorId )
    {
        // Get machineId & subscription pointer
        Machine * machine = dataController->getMachineBySensor( sensorId );

        if ( machine == nullptr )
        {
            logger->writeWarn( std::string( "" ) + typeid(*this).name() + ": Machine for Sensor with ID '" + std::to_string( sensorId ) + "' could not be found. Nothing to unsubscribe from." );

            return false;
        }
        
        int machineId = machine->getID();
        Client * client = getClient( machineId );

        // If the corresponding Client exists, remove subscription.
        if ( client != nullptr )
        {
            // Unsubscribe from DataChange events for this sensor
            OpcUa::Subscription::SharedPtr subscription = client->subscription;

            if ( subscription.get() != nullptr )
            {
                unsigned int handle = getSubscriptionHandle( sensorId );
                subscription->UnSubscribe( handle );
                handles.erase( sensorId );

                logger->writeInfo( std::string( "" ) + typeid(*this).name() + ": Deleted Subscription for Sensor with ID '" + std::to_string( sensorId ) + "', Handle: '" + std::to_string( handle ) +  "'." );
            }
            else
            {
                logger->writeWarn( std::string( "" ) + typeid(*this).name() + ": Sensor with ID '" + std::to_string( sensorId ) + "' has no Subscription; did not delete the Subscription." );
            }

            return true;
        }
        else
        {
            logger->writeWarn( std::string( "" ) + typeid(*this).name() + ": No Sensor to unsubscribe from (ID: " + std::to_string( sensorId ) + "); Couldn't find Client for Machine with ID '" + std::to_string( machineId ) + "'." );
        
            return false;
        }
    }

    std::vector<int> Listener::addClients()
    {
        std::map<int, Machine *> * machines = dataController->getMachinesContainer();
        std::vector<int> machineList;

        // Check for new Machines
        for ( std::map<int, Machine *>::iterator it = machines->begin(); it != machines->end(); ++it )
        {
            int machineId = it->first;
            Machine * machine = it->second;

            // If Client doesn't exist yet, but machine is marked as active -> addClient.
            if ( getClient( machineId ) == nullptr && machine->isActive() )
            {
                Client * client = addClient( machineId );
                
                // If a client has been added, append ID to list.
                if ( client != nullptr )
                {
                    machineList.push_back( machineId );
                }
            }
        }

        return machineList;
    }

    std::vector<int> Listener::deleteClients()
    {
        std::vector<int> machineList;

        for (  std::map<int, Client *>::iterator it = clients.begin(); it != clients.end(); ++it )
        {
            int machineId = it->first;

            // This needs to be saved, because the current entry is being deleted, and thus it will point to null.
            std::map<int, Client *>::iterator next = std::next( it, 1 );
           
            // If a client has been deleted, append negated ID to list.
            if ( deleteClient( machineId ) )
            {
                it = next;

                machineList.push_back( machineId * -1 );
            }
        }

        return machineList;
    }

    std::vector<int> Listener::updateClients()
    {
        std::vector<int> machineList;

        // Update current machines
        for ( std::map<int, Client *>::iterator it = clients.begin(); it != clients.end(); ++it )
        {
            int machineId = it->first;
            Machine * machine = dataController->getMachine( machineId );

            // If machine has been removed, OR if Client exists, but machine is marked as not (!) active -> deleteClient.
            if ( machine == nullptr || ( getClient( machineId ) != nullptr && !machine->isActive() ) )
            {
                deleteSubscriptions( machineId );

                // This needs to be saved, because the current entry is being deleted, and thus it will point to null.
                std::map<int, Client *>::iterator next = std::next( it, 1 );

                // If a client has been deleted, append negated ID to list.
                if ( deleteClient( machineId ) )
                {
                    it = next;

                    machineList.push_back( machineId * -1 );
                }
            }

            // Update Subscriptions for clients already existing
            updateSubscriptions( machineId );
        }

        // Add new Machines
        std::vector<int> temp = addClients();
        machineList.insert( machineList.end(), temp.begin(), temp.end() );

        return machineList;
    }

    std::vector<int> Listener::addSubscriptions( int machineId )
    {
        std::vector<int> sensorList;
        Machine * machine = dataController->getMachine( machineId );

        if ( machine != nullptr && getClient( machineId ) != nullptr )
        {
            std::map<int, Sensor *> sensors = dataController->getSensorsContainer( machineId );

            // Check for new Sensors
            for ( std::map<int, Sensor *>::iterator it = sensors.begin(); it != sensors.end(); ++it )
            {
                int sensorId = it->first;
                Sensor * sensor = it->second;

                // If subscription doesn't exist yet, but sensor is marked as active -> addSubscription
                if ( getSubscriptionHandle( sensorId ) == 0 && sensor != nullptr && sensor->isActive() )
                {
                    unsigned int handle = addSubscription( sensorId );

                    // If a subscription has been added, append ID to list.
                    if ( handle != 0 )
                    {
                        sensorList.push_back( sensorId );
                    }
                }
            }
        }
        else
        {
            logger->writeInfo( std::string( "" ) + typeid(*this).name() + ": Machine with ID '" + std::to_string( machineId ) + "' is not active; did not add new Subscriptions." );
        }

        return sensorList;
    }

    std::vector<int> Listener::deleteSubscriptions( int machineId )
    {
        std::vector<int> sensorList;

        // Remove deleted/deactivated sensors
        for ( std::map<int, unsigned int>::iterator it = handles.begin(); it != handles.end(); ++it )
        {
            Machine * machine = dataController->getMachine( machineId );
            int sensorId = it->first;
            Sensor * sensor = dataController->getSensor( sensorId );

            // If sensor has been removed, OR if Sensor exists on specific machine, subscription exists, but sensor is marked as not (!) active -> deleteSubscription.
            if ( ( sensor == nullptr ) || ( sensor->getMachineID() == machineId && getSubscriptionHandle( sensorId ) != 0 && !sensor->isActive() ) || ( sensor->getMachineID() == machineId && ( machine == nullptr || !machine->isActive() ) ) )
            {
                // This needs to be saved, because the current entry is being deleted, and thus it will point to null.
                std::map<int, unsigned int>::iterator next = std::next( it, 1 );

                // If a Subscription has been deleted, append negated ID to list.
                if ( deleteSubscription( sensorId ) )
                {
                    it = next;

                    sensorList.push_back( sensorId * -1 );
                }
            }
        }

        return sensorList;
    }

    std::vector<int> Listener::updateSubscriptions( int machineId )
    {
        std::vector<int> sensorList;

        // Remove sensors
        std::vector<int> temp = deleteSubscriptions( machineId );
        sensorList.insert( sensorList.end(), temp.begin(), temp.end() );

        // Add new sensors
        temp = addSubscriptions( machineId );
        sensorList.insert( sensorList.end(), temp.begin(), temp.end() );

        return sensorList;
    }
} // namespace opcd