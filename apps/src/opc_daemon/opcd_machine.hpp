#ifndef OPCD_MACHINE_HPP
#define OPCD_MACHINE_HPP

// Standard Libraries
#include <string>

// OpcUa
#include <opc/ua/protocol/status_codes.h>

// opcd
#include "opcd_exceptions.hpp"

namespace opcd
{
    class Machine
    {
    private:
        int id;
        std::string uri;
        std::string name;
        bool active;
        std::string creationTime;
        std::string location;
        std::string description;
        OpcUa::StatusCode status;
        unsigned int numSensors;

        void setID( int id );
        void setURI( std::string uri );
        void setCreationTime( std::string creationTime );
        
    public:
        Machine(    int id,
                    std::string uri,
                    std::string name,
                    bool active,
                    std::string creationTime,
                    std::string location,
                    std::string description
        );
        
        int getID();
        std::string getURI();
        void setName( std::string name );
        std::string getName();
        void setActive( bool active );
        bool isActive();
        std::string getCreationTime();
        void setLocation( std::string location );
        std::string getLocation();
        void setDescription( std::string description );
        std::string getDescription();
        void setStatus( OpcUa::StatusCode status );
        OpcUa::StatusCode getStatus();
        bool isStatusGood();
        void setNumSensors( unsigned int numSensor );
        unsigned int getNumSensors();
    };
} // namespace opcd

#endif // OPCD_MACHINE_HPP