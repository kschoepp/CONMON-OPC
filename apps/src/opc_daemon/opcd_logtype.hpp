#ifndef OPCD_LOGTYPE_HPP
#define OPCD_LOGTYPE_HPP

// Standard Libraries
#include <string>

namespace opcd
{
    class LogType
    {
    public:
        const static std::string Info;
        const static std::string Warn;
        const static std::string Debug;
        const static std::string Error;
        const static std::string All;
    };
}

#endif // OPCD_LOGTYPE_HPP