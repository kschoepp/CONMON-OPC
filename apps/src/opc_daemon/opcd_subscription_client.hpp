#ifndef OPCD_SUBSCRIPTION_CLIENT_HPP
#define OPCD_SUBSCRIPTION_CLIENT_HPP

// Standard Libraries
#include <string>

// OpcUa
#include <opc/ua/client/client.h>
#include <opc/ua/node.h>
#include <opc/ua/protocol/status_codes.h>
#include <opc/ua/protocol/string_utils.h>
#include <opc/ua/protocol/datetime.h>

// PostgreSQL Libs
#include <pqxx/pqxx>

// opcd
#include "opcd_daemon.hpp"
#include "opcd_data_controller.hpp"
#include "opcd_exceptions.hpp"

namespace opcd
{
    class Daemon;
    class Logger;
    class DataController;

    class SubscriptionClient : public OpcUa::SubscriptionHandler
    {        
    private:
        Daemon * daemon;
        Config * config;
        Logger * logger;
        DataController * dataController;

        int machineId;

        void setDaemonInstance( Daemon * daemon );
        void setMachineID( int machineId );
        void DataChange( unsigned int handle, const OpcUa::Node & node, const OpcUa::Variant & val, OpcUa::AttributeId attr ) override;
        void DataValueChange( unsigned int handle, const OpcUa::Node & node, const OpcUa::DataValue & val, OpcUa::AttributeId attr ) override;
        void StatusChange( OpcUa::StatusCode status ) override;

    public:
        SubscriptionClient( Daemon * daemon, int machineId );

        Daemon * daemonInstance();
        int getMachineID();
        bool isValueNumeric( OpcUa::Variant value );
    };
} // namespace opcd

#endif // OPCD_SUBSCRIPTION_CLIENT_HPP