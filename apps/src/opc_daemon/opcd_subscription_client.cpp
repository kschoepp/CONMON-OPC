#include "opcd_subscription_client.hpp"

namespace opcd
{
	SubscriptionClient::SubscriptionClient( Daemon * daemon, int machineId )
	{
		setDaemonInstance( daemon );
		config = daemonInstance()->configInstance();
		logger = daemonInstance()->loggerInstance();
        dataController = daemonInstance()->dataControllerInstance();
		setMachineID( machineId );
	}

	void SubscriptionClient::SubscriptionClient::setDaemonInstance( Daemon * daemon )
	{
		if ( daemon == nullptr )
		{
			throw OPCDDaemonInstanceEmptyException( __FILE__, __LINE__ );
		}

		this->daemon = daemon;
	}

	Daemon * SubscriptionClient::daemonInstance()
	{
		return daemon;
	}

	void SubscriptionClient::setMachineID( int machineId )
	{
		this->machineId = machineId;
	}

	int SubscriptionClient::getMachineID()
	{
		return machineId;
	}

	void SubscriptionClient::DataChange( unsigned int handle, const OpcUa::Node & node, const OpcUa::Variant & val, OpcUa::AttributeId attr )
	{
		return;
	}

	void SubscriptionClient::DataValueChange( unsigned int handle, const OpcUa::Node & node, const OpcUa::DataValue & dataValue, OpcUa::AttributeId attr )
	{
		Sensor * sensor = dataController->getSensor( getMachineID(), node.GetId().GetNamespaceIndex(), node.GetId().GetIntegerIdentifier() );
		
		if ( sensor != nullptr )
		{
			int sensorId = sensor->getID();
			OpcUa::Variant value = dataValue.Value;
			time_t time = OpcUa::DateTime::ToTimeT( dataValue.ServerTimestamp );

			if ( isValueNumeric( value ) )
			{
				// TODO: Check if new value exceeds sensor.getMinDelta()
			}	

			opcd::DatabaseConnector dbConnector( (*config)["DB_HOST"], (*config)["DB_PORT"], (*config)["DB_NAME"], (*config)["DB_USER"], (*config)["DB_PASSWORD"] );
			dbConnector.connect();
			dbConnector.query(
				"INSERT INTO " + (*config)["DB_SENSORDATA_TABLE"] + " ( " + (*config)["DB_SENSORDATA_COLUMN_ID"] + ", " + (*config)["DB_SENSORDATA_COLUMN_VALUE"] + ", " + (*config)["DB_SENSORDATA_COLUMN_TIMESTAMP"] + " )"
				"VALUES (" 
				+ std::to_string( sensorId ) + ", "
				+ value.ToString() + ", "
				+ "to_timestamp( " + std::to_string( time ) + " )"
				");"
			);

			std::cout << "- New Value for " << sensorId << " is: " << value.ToString() << std::endl;
		}
		else
		{
			logger->writeWarn( "A Sensor has been deleted during a DataChange event. Discarding the Data.." );
		}
	}

	void SubscriptionClient::StatusChange( OpcUa::StatusCode status )
	{
		std::cout << "- New Connection Status for " << std::to_string( getMachineID() ) << " is: " << OpcUa::ToString( status ) << std::endl;

		Machine * machine = dataController->getMachine( getMachineID() );
			machine->setStatus( status );

		opcd::DatabaseConnector dbConnector( (*config)["DB_HOST"], (*config)["DB_PORT"], (*config)["DB_NAME"], (*config)["DB_USER"], (*config)["DB_PASSWORD"] );
			dbConnector.connect();
			dbConnector.query(
				"INSERT INTO " + (*config)["DB_MACHINEDATA_TABLE"] + " ( " + (*config)["DB_MACHINEDATA_COLUMN_ID"] + ", " + (*config)["DB_MACHINEDATA_COLUMN_CONNECTED"] + ", " + (*config)["DB_MACHINEDATA_COLUMN_TIMESTAMP"] + " )"
				"VALUES (" 
				+ std::to_string( getMachineID() ) + ", "
				+ std::to_string( machine->isStatusGood() ) + ", "
				+ "NOW()"
				");"
			);
	}

	bool SubscriptionClient::isValueNumeric( OpcUa::Variant value )
	{
		OpcUa::VariantType type = value.Type();

		return (	type == OpcUa::VariantType::SBYTE ||type == OpcUa::VariantType::BYTE || type == OpcUa::VariantType::INT16 || type == OpcUa::VariantType::UINT16 ||
					type == OpcUa::VariantType::INT32 || type == OpcUa::VariantType::UINT32 || type == OpcUa::VariantType::INT64 || type == OpcUa::VariantType::UINT64 ||
					type == OpcUa::VariantType::FLOAT || type == OpcUa::VariantType::DOUBLE );
	}
} // namespace opcd