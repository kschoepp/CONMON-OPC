#ifndef OPCD_CLIENT_HPP
#define OPCD_CLIENT_HPP

// Standard Libraries
#include <string>

// OpcUa
#include <opc/ua/client/client.h>
#include <opc/ua/node.h>
#include <opc/ua/subscription.h>

// opcd
#include "opcd_exceptions.hpp"
#include "opcd_subscription_client.hpp"

namespace opcd
{
    class SubscriptionClient;
    
    class Client
    {
    public:
        OpcUa::UaClient * instance;
        SubscriptionClient * subscriptionClient;
        OpcUa::Subscription::SharedPtr subscription;

        Client( OpcUa::UaClient * instance, SubscriptionClient * subscriptionClient, OpcUa::Subscription::SharedPtr subscription );
        ~Client();
    };
} // namespace opcd

#endif // OPCD_CLIENT_HPP