/// @brief OPC UA Server main.
/// @license GNU LGPL
///
/// Distributed under the GNU LGPL License
/// (See accompanying file LICENSE or copy at
/// http://www.gnu.org/licenses/lgpl.html)
///
#include <iostream>
#include <algorithm>
#include <time.h>

#include <thread>         
#include <chrono>   

#include <opc/ua/node.h>
#include <opc/ua/subscription.h>
#include <opc/ua/server/server.h>


using namespace OpcUa;

class SubClient : public SubscriptionHandler
{
  void DataChange(uint32_t handle, const Node& node, const Variant& val, AttributeId attr) override
  {
    std::cout << "Received DataChange event for Node " << node << std::endl;
  }
};

std::vector<OpcUa::Variant> GetTime( NodeId context, std::vector<OpcUa::Variant> arguments )
{
  std::cout << "GetTime called! " << std::endl;

  std::time_t date;
  std::time( &date );
  std::string dateStr = ctime( &date );
  std::string res = "-----The Server-Time is: " + dateStr;

  std::vector<OpcUa::Variant> result;
  result.push_back( Variant( res ) );

  return result;
}

void RunServer( std::string endpoint )
{
  //First setup our server
  const bool debug = false;
  OpcUa::UaServer server( debug );
  std::cout << endpoint << std::endl;
  
  server.SetEndpoint( "opc.tcp://" + endpoint + "/" );
  server.SetServerURI( "urn://exampleserver.freeopcua.github.io" );
  server.Start();
  
  //then register our server namespace and get its index in server
  uint32_t idx = server.RegisterNamespace( "demo_server" );
  
  //Create our address space using different methods
  Node objects = server.GetObjectsNode();
  
  //Add a custom object with specific nodeid
  NodeId nid( 99, idx );
  QualifiedName qn( "NewObject", idx );
  Node newobject = objects.AddObject( nid, qn );

  //Add a variable and a property with auto-generated nodeid to our custom object
  Node counter = newobject.AddVariable( idx, "counterVar", Variant(8) );
  Node time = newobject.AddMethod( idx, "GetTime", GetTime );

  //browse root node on server side
  Node root = server.GetRootNode();
  std::cout << "Root node is: " << root << std::endl;
  std::cout << "Childs are: " << std::endl;

  for ( Node node : root.GetChildren() )
  {
    std::cout << "    " << node << std::endl;
  }

  //Now write values to address space and send events so clients can have some fun
  uint32_t count = 0;
  counter.SetValue( Variant( count ) ); //will change value and trigger datachange event

  //Create event
  server.EnableEventNotification();
  Event ev( ObjectId::BaseEventType ); //you should create your own type
  ev.Severity = 2;
  ev.SourceNode = ObjectId::Server;
  ev.SourceName = "Event from FreeOpcUA";
  ev.Time = DateTime::Current();

	std::cout << "dfsgvd345345345234324fgdfgs" << std::endl;
  std::cout << "Ctrl-C to exit" << std::endl;

  for (;;)
  {
    counter.SetValue( Variant( ++count ) ); //will change value and trigger datachange event
    std::stringstream ss;
    ss << "This is event number: " << counter;
    ev.Message = LocalizedText( ss.str() );
    server.TriggerEvent( ev );
    std::this_thread::sleep_for( std::chrono::milliseconds( 5000 ));
  }

  server.Stop();
}

int main( int argc, char* argv[] )
{
  try
  {
	std::string endpoint = argv[1];
    RunServer( endpoint );
  }
  catch ( const std::exception& exc )
  {
    std::cout << exc.what() << std::endl;
  }
  return 0;
}

