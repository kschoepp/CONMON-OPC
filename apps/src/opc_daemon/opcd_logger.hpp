#ifndef OPCD_LOGGER_HPP
#define OPCD_LOGGER_HPP

// Standard Libraries
#include <iostream> // std::cout, std::endl
#include <iomanip> // std::put_time
#include <fstream> // std::ofstream
#include <sstream> // std::stringstream
#include <string> //std::string
#include <ctime> // std::localtime
#include <chrono> // std::chrono::system_clock
#include <sys/stat.h> // ::mkdir
#include <sys/types.h> // ::mkdir

// opcd
#include "opcd_logtype.hpp" // opcd::LogType

namespace opcd
{
    class Logger
    {
    private:
        std::string name;
        std::string path;
        bool active;
        bool consoleOutputActive;

        void getStreamObject( std::string type, std::ofstream & file );
        void makeFile( std::string type );
        void validateFile( std::string type );
        void validateFiles();
        void write( std::string type, std::string msg );

    public:
        Logger( std::string name, std::string path = "./logs/", bool active = true, bool consoleOutputActive = true );
        
        std::string trimLine( std::string line );
        void setName( std::string name );
        std::string getName();
        void setPath( std::string path);
        std::string getPath();
        void setActive( bool active );
        bool isActive();
        void setConsoleOutputActive( bool active );
        bool isConsoleOutputActive();
        std::string getFilename( std::string type );
        bool fileExists( std::string type );
        void writeInfo( std::string msg );
        void writeWarn( std::string msg );
        void writeDebug( std::string msg );
        void writeError( std::string msg );
    };
} // namespace opcd

#endif // OPCD_LOGGER_HPP