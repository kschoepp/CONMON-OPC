#ifndef OPCD_EXCEPTIONS_HPP
#define OPCD_EXCEPTIONS_HPP

// Standard Libraries
#include <string>
#include <iostream>

namespace opcd
{
    class OPCDException //: public std::exception - Couldn't parse char * in cout correctly
    {
    private:
        std::string file;
        int line;
        std::string msg;
        
    public:
        OPCDException( std::string file, int line, std::string msg );
        OPCDException( std::string file, int line );
        OPCDException( std::string msg );
        OPCDException();

        void setFile( std::string file );
        std::string getFile();
        void setLine( int line );
        int getLine();
        std::string getFileInfo();
        void setMsg( std::string msg );
        std::string getMsg();
        virtual std::string what() throw();
    };
    
    class OPCDConfigMalformedException : private OPCDException
    {
    public:
        using OPCDException::OPCDException;

        std::string what() throw();
    };

    class OPCDConfigMissingException : private OPCDException
    {
    public:
        using OPCDException::OPCDException;
        
        std::string what() throw();
    };

    class OPCDConfigKeyNotFoundException : private OPCDException
    {
    public:
        using OPCDException::OPCDException;
        
        std::string what() throw();
    };

    class OPCDDatabaseNotConnectedException : private OPCDException
    {
    public:
        using OPCDException::OPCDException;
        
        std::string what() throw();
    };

    class OPCDEmptyFieldException : private OPCDException
    {
    public:
        using OPCDException::OPCDException;
        
        std::string what() throw();
    };
    
    class OPCDDaemonInstanceEmptyException : private OPCDException
    {
    public:
        using OPCDException::OPCDException;
        
        std::string what() throw();
    };

    class OPCDDataTargetTypeEmptyException : private OPCDException
    {
    public:
        using OPCDException::OPCDException;
        
        std::string what() throw();
    };

    class OPCDClockTooLowException : private OPCDException
    {
    public:
        using OPCDException::OPCDException;
        
        std::string what() throw();
    };
} // namespace opcd

#endif // OPCD_EXCEPTIONS_HPP