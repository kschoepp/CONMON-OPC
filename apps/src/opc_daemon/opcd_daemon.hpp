#ifndef OPCD_DAEMON_HPP
#define OPCD_DAEMON_HPP

// Standard Libraries
#include <string>
#include <typeinfo>
#include <stdexcept>
#include <iostream>
#include <atomic> // std::atomic
#include <thread> // std::thread, std::this_thread::sleep_for

// opcd
#include "opcd_config.hpp"
#include "opcd_data_target.hpp"
#include "opcd_data_controller.hpp"
#include "opcd_database_connector.hpp"
#include "opcd_exceptions.hpp"
#include "opcd_listener.hpp"
#include "opcd_logger.hpp"
#include "opcd_machine.hpp"
#include "opcd_sensor.hpp"

namespace opcd
{
    class DataController;
    class Listener;

    class Daemon
    {        
    private:
        bool started;
        std::atomic<bool> updateLoopActive;

        // Modules
        std::string configFile;
        Config * config;
        Logger * logger;
        DatabaseConnector * databaseConnector;
        DataController * dataController;
        Listener * listener;
        
    public:
        Daemon( std::string configFile );
        ~Daemon();

        void setStarted( bool started );
        bool isStarted();
        void setConfigFile( std::string configFile );
        std::string getConfigFile();
        void initConfig();
        Config * configInstance();
        void deleteConfig();
        void initLogger();
        Logger * loggerInstance();
        void deleteLogger();
        void initDatabaseConnector();
        DatabaseConnector * databaseConnectorInstance();
        void deleteDatabaseConnector();
        void initDataController();
        DataController * dataControllerInstance();
        void deleteDataController();
        void initListener();
        Listener * listenerInstance();
        void deleteListener();

        void initModules();
        void deleteModules();
        
        void boot();
        void shutdown();
        void restart();

        void setUpdateLoopActive( bool active );
        std::atomic<bool> isUpdateLoopActive();
        void startUpdateLoop();
        void stopUpdateLoop();
        void updateLoop();
        static void updateLoop_Static( Daemon * daemon );
    };
} // namespace opcd

#endif // OPCD_DAEMON_HPP