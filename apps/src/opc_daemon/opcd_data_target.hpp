#ifndef OPCD_DATA_TARGET_HPP
#define OPCD_DATA_TARGET_HPP

// Standard Libraries
#include <string>

// opcd
#include "opcd_exceptions.hpp"

namespace opcd
{
    class DataTarget
    {
    private:
        int id;
        std::string type;
        std::string destination;
        std::string username;
        std::string password;
        std::string description;
        bool connected;
        bool active;

        void setID( int id );
        void setType( std::string type );
        void setDestination( std::string destination );
        void setUsername( std::string username );
        void setPassword( std::string password );
        void setDescription( std::string description );

    public:
        DataTarget( int id,
                    std::string type,
                    std::string destination,
                    std::string username,
                    std::string password,
                    bool connected,
                    std::string description
        );
        
        int getID();
        std::string getType();
        std::string getDestination();
        std::string getUsername();
        std::string getPassword();
        std::string getDescription();
        void setConnected( bool connected );
        bool isConnected();
        void setActive( bool active );
        bool isActive();
    };
} // namespace opcd

#endif // OPCD_DATA_TARGET_HPP