#include "opcd_logger.hpp"

namespace opcd
{
    Logger::Logger( std::string name, std::string path, bool active, bool consoleOutputActive )
    {
        setName( name );
        setPath( path );
        validateFiles();
        setActive( active );
        setConsoleOutputActive( consoleOutputActive );
    }
    
    std::string Logger::trimLine( std::string line )
    {
        size_t first = line.find_first_not_of(' ');

        if ( first == std::string::npos )
        {
            return "";
        }

        size_t last = line.find_last_not_of(' ');

        return line.substr( first, ( last - first + 1 ) );
    }

    void Logger::setName( std::string name )
    {
        this->name = trimLine( name );
    }

    std::string Logger::getName()
    {
        return this->name;
    }

    void Logger::setPath( std::string path )
    {
        path = trimLine( path );

        if ( path.back() != '/' )
        {
            path += '/';
        }

        this->path = path;
    }

    std::string Logger::getPath()
    {
        return this->path;
    }

    void Logger::setActive( bool active )
    {
        this->active = active;
    }

    bool Logger::isActive()
    {
        return active;
    }

    void Logger::setConsoleOutputActive( bool consoleOutputActive )
    {
        this->consoleOutputActive = consoleOutputActive;
    }

    bool Logger::isConsoleOutputActive()
    {
        return consoleOutputActive;
    }

    std::string Logger::getFilename( std::string type )
    {
        std::string filename = std::string( getPath() + getName() + "_" + type + ".log" );

        return filename;
    }

    void Logger::getStreamObject( std::string type, std::ofstream& file )
    {
        file.open( getFilename( type ), std::ofstream::out | std::ofstream::app );
    }

    void Logger::makeFile( std::string type )
    {
        // Make directory, if it doesn't already exist
        ::mkdir( getPath().c_str(), 0777 );

        // Create file, if it doesn't already exist
        std::ofstream file;
        getStreamObject( type, file );
        file.close();
    }

    bool Logger::fileExists( std::string type )
    {
        if ( std::ifstream( getFilename( type ) ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void Logger::validateFile( std::string type )
    {
        if ( !fileExists( type ) )
        {
            makeFile( type );
        }
    }

    void Logger::validateFiles()
    {
        validateFile( LogType::Info );
        validateFile( LogType::Warn );
        validateFile( LogType::Debug );
        validateFile( LogType::Error );
    }

    void Logger::write( const std::string type, const std::string msg )
    {
        if ( !isActive() )
        {
            return;
        }
        
        std::time_t timeNow = std::chrono::system_clock::to_time_t( std::chrono::system_clock::now() );

        validateFile( type );

        std::stringstream logLine;
            logLine << "[" << std::put_time( std::localtime( &timeNow ), "%c" ) << "] " << type << " : " << msg;

        if ( this->consoleOutputActive )
        {
            std::cout << logLine.str() << std::endl;
        }
              
        std::ofstream file;
        getStreamObject( type, file );

        if ( file.is_open() )
        {
            file << logLine.str() << std::endl;
            file.close();
        }
        else
        {
            std::cout << "Unable to open file: " << getFilename( type ) << std::endl;
        }  
    }
    
    void Logger::writeInfo( std::string msg )
    {
        write( LogType::Info, msg );
    }

    void Logger::writeWarn( std::string msg )
    {
        write( LogType::Warn, msg );
    }
    void Logger::writeDebug( std::string msg )
    {
        write( LogType::Debug, msg );
    }
    
    void Logger::writeError( std::string msg )
    {
        write( LogType::Error, msg );
    }
} // namespace opcd