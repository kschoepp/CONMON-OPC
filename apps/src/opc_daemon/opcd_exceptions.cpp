#include "opcd_exceptions.hpp"

namespace opcd
{
    OPCDException::OPCDException( std::string file, int line, std::string msg )
    {
        setFile( file );
        setLine( line );
        setMsg( msg );
    }

    OPCDException::OPCDException( std::string file, int line )
    {
        OPCDException( file, line, "" );
    }

    OPCDException::OPCDException( std::string msg )
    {
        OPCDException( "", 0, msg );
    }

    OPCDException::OPCDException()
    {
        OPCDException( "", 0, "" );
    }

    void OPCDException::setMsg( std::string msg )
    {
        this->msg = msg;
    }

    std::string OPCDException::getMsg()
    {
        return msg;
    }

    void OPCDException::setFile( std::string file )
    {
        this->file = file;
    }

    std::string OPCDException::getFile()
    {
        return file;
    }

    void OPCDException::setLine( int line )
    {
        this->line = line;
    }

    int OPCDException::getLine()
    {
        return line;
    }

    std::string OPCDException::getFileInfo()
    {
        return "File: " + getFile() + ", Line: " + std::to_string( getLine() );
    }

    std::string OPCDException::what() throw()
    {
        return "" + getFileInfo() + ": Message: " + getMsg();
    }
    
    std::string OPCDConfigMalformedException::what() throw()
    {
        return "Config file is malformed: " + getMsg();
    }

    std::string OPCDConfigMissingException::what() throw()
    {
        return " Config file is missing: " + getMsg();
    }

    std::string OPCDConfigKeyNotFoundException::what() throw()
    {
        return "Key '" + getMsg() + "' was not found in the config file."; 
    }

    std::string OPCDDatabaseNotConnectedException::what() throw()
    {
        return "" + getFileInfo() + ": Database is not yet connected. (Has connect() been called?)";
    }
    
    std::string OPCDEmptyFieldException::what() throw()
    {
        return "" + getFileInfo() + ": Empty field in DB Table: " + getMsg();
    }

    std::string OPCDDaemonInstanceEmptyException::what() throw()
    {
        return "" + getFileInfo() + ": Daemon instance must not be empty; Non-Nullpointer to instance is required.";
    } 

    std::string OPCDDataTargetTypeEmptyException::what() throw()
    {
        return "Type of DataTarget must not be empty. ID: " + getMsg();
    }

    std::string OPCDClockTooLowException::what() throw()
    {
        return "" + getFileInfo() + ": Clock must be above 0.";
    } 
} // namespace opcd