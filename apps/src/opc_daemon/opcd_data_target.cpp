#include "opcd_data_target.hpp"

namespace opcd
{
    DataTarget::DataTarget( int id, std::string type, std::string destination, std::string username, std::string password, bool connected, std::string description )
    {
        setID( id );
        setType( type );
        setDestination( destination );
        setUsername( username );
        setPassword( password );
        setDescription( description );
        setConnected( connected );
    }

    void DataTarget::setID( int id )
    {
        this->id = id;
    }

    int DataTarget::getID()
    {
        return id;
    }

    void DataTarget::setType( std::string type )
    {
        if ( type.empty() )
        {
            throw OPCDDataTargetTypeEmptyException( std::to_string( id ) );
        }

        this->type = type;
    }

    std::string DataTarget::getType()
    {
        return type;
    }

    void DataTarget::setDestination( std::string destination )
    {
        this->destination = destination;
    }

    std::string DataTarget::getDestination()
    {
        return destination;
    }

    void DataTarget::setUsername( std::string username )
    {
        this->username = username;
    }

    std::string DataTarget::getUsername()
    {
        return username;
    }

    void DataTarget::setPassword( std::string password )
    {
        this->password = password;
    }

    std::string DataTarget::getPassword()
    {
        return password;
    }

    void DataTarget::setDescription( std::string description )
    {
        this->description = description;
    }

    std::string DataTarget::getDescription()
    {
        return description;
    }

    void DataTarget::setConnected( bool connected )
    {
        this->connected = connected;
    }

    bool DataTarget::isConnected()   
    {
        return connected;
    }

    void DataTarget::setActive( bool active )
    {
        this->active = active;
    }
    bool DataTarget::isActive()
    {
        return active;
    }
} // namespace opcd