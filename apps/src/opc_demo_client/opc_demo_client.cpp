#include <iostream>
#include <stdexcept>
#include <thread>

// FreeOpcUa Libs
#include <opc/ua/client/client.h>
#include <opc/ua/node.h>
#include <opc/ua/subscription.h>

//PostgreSQL Libs
#include <pqxx/pqxx>

// Subscription Client, which prints something for all our Data Changes
class SubClient : public OpcUa::SubscriptionHandler
{
	void DataChange( uint32_t handle, const OpcUa::Node & node, const OpcUa::Variant & val, OpcUa::AttributeId attr ) override
	{
		std::cout << "New Value for " << node.GetBrowseName().Name << " is: " << val.ToString() << std::endl;
		
		/*std::string connectionString = "host = ingence.de port = 25432 dbname = conmon user = postgres password = psql";
		pqxx::connection dbConnection( connectionString );
		pqxx::work work( dbConnection );
		
		work.exec(
			"INSERT INTO sensorData (sid, value, ts)"
			"VALUES (" 
			+ std::to_string( node.GetId().GetIntegerIdentifier() ) + ", "
			+ val.ToString() + ", "
			+ "NOW()"
			");"
		);
		work.commit();*/
	}
};

int main( int argc, char* argv[] )
{
	try
	{
		std::string protocol = "ocp.tcp";
		std::string endpoint = argv[1];

		// Init Client
		std::cout << "-----Connecting to: " << protocol + "://" + endpoint + "/" << std::endl;

		bool debug = false;
		OpcUa::UaClient client( debug );
    		client.Connect( protocol + "://" + endpoint + "/" );

		// Get some Info
		OpcUa::Node root = client.GetRootNode();
		OpcUa::Node objects = client.GetObjectsNode();
		uint32_t idx = client.GetNamespaceIndex( "demo_server" );

		// Get Variables
		int numVars = 11;
		OpcUa::Node demoVars[numVars];
		
		for ( int i = 0; i < numVars; i++ )
		{
			std::vector<std::string> varPath{ std::to_string( idx ) + ":DemoObject", "demoVar" + std::to_string( i ) };
			demoVars[i] = objects.GetChild( varPath );
		}

		// Subscribe to DataChange Events for all our Variables
		SubClient subClient;
		OpcUa::Subscription::SharedPtr sub = client.CreateSubscription( 100, subClient );
		
		for ( int i = 0; i < numVars; i++ )
		{
			if ( demoVars[i].IsValid() )
			{
				uint32_t handle = sub->SubscribeDataChange( demoVars[i] );
				std::cout << "Got sub handle: " << handle << std::endl;
			}
			else
			{
				std::cout << "NONONONON" << std::endl;
			}
		}

		//
		OpcUa::NodeId nid( 2012, 2 );
		OpcUa::Node invalidNode = client.GetNode( nid );

		if ( invalidNode.IsValid() )
		{
			uint32_t handle = sub->SubscribeDataChange( invalidNode );
			std::cout << "Got sub handle: " << handle << std::endl;
		}
		else
		{
			std::cout << "Couldn't subscribe to node: " << invalidNode << std::endl;
		}
		
		// Infinite Loop
		std::cout << "Ctrl-C to exit" << std::endl;

		for (;;)
		{
			std::cout << "--------------------" << std::endl;
	
			std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
		}

		std::cout << "-----Disconnecting" << std::endl;
		client.Disconnect();

		return 0;
	}
	catch ( const std::exception& e )	{
		std::cout << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "-----Unknown error." << std::endl;
	}
	
	return -1;
}

