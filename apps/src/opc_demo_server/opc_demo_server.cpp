#include <iostream>
#include <algorithm>
#include <stdlib.h>
#include <time.h>
#include <thread>         
#include <chrono>   

#include <opc/ua/node.h>
#include <opc/ua/subscription.h>
#include <opc/ua/server/server.h>

void RunServer( std::string protocol, std::string endpoint )
{
	// Init Server
	const bool debug = false;
	OpcUa::UaServer server( debug );  
		server.SetEndpoint( protocol + "://" + endpoint + "/" );
		server.SetServerURI( "urn://" + endpoint );
		server.Start();

	uint32_t idx = server.RegisterNamespace( "demo_server" ); // get namespace index
	OpcUa::Node objects = server.GetObjectsNode(); // get Objects Node
  
	// Add custom Object with specific NodeID
	OpcUa::QualifiedName qn( "DemoObject", idx );
	OpcUa::NodeId nid( 100, idx );
	OpcUa::Node demoObject = objects.AddObject( nid, qn );

	// Add vars to Object
	int numVars = 11;
	OpcUa::Node demoVars[numVars];
	
	for ( int i = 0; i < numVars; i++ )
	{

		std::string name = "demoVar" + std::to_string( i );
		demoVars[i] = demoObject.AddVariable( idx, name, OpcUa::Variant(0) );
	}

	// Set random values
	std::cout << "Ctrl-C to exit" << std::endl;
	std::srand( time( NULL ) );

	for (;;)
	{

		for ( int i = 0; i < numVars; i++ )
		{
			// only set new values if the number is under 2000 (20% chance)
			if ( std::rand() % 10000 < 2000 )
			{
				demoVars[i].SetValue( OpcUa::Variant( std::rand() % 1000 ) );

				std::cout << "New Value for " << demoVars[i].GetBrowseName().Name << " (NS=" << demoVars[i].GetId().GetNamespaceIndex() << ", ID=" << demoVars[i].GetId().GetIntegerIdentifier() << ") " << " is: " << demoVars[i].GetValue().ToString() << std::endl;
			}
		}

		std::this_thread::sleep_for( std::chrono::milliseconds( 1000 ) );
	}

	server.Stop();
}

int main( int argc, char* argv[] )
{
	try
	{
		std::string protocol = "ocp.tcp";
		std::string endpoint = argv[1];
		RunServer( protocol, endpoint );
	}
	catch ( const std::exception& e )
	{
		std::cout << e.what() << std::endl;
	}
	return 0;
}

