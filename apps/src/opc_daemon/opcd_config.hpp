#ifndef OPCD_CONFIG_HPP
#define OPCD_CONFIG_HPP

// Standard Libraries
#include <map> // std::map
#include <sstream>
#include <fstream> // std::istringstream
#include <string> // std::string, std::getline, std::string::npos
#include <algorithm> // std::count

// opcd
#include "opcd_exceptions.hpp" // opcd::ConfigMalformedException

namespace opcd
{
    class Config
    {
    private:
        char delimiter;
        char commentDelimiter;
        
        void setDelimiter( char delimiter );
        char getDelimiter();
        void setCommentDelimiter( char commentDelimiter );
        char getCommentDelimiter();
        void getStreamObject( std::string filename, std::ifstream & file );
        bool fileExists( std::string filename );
        bool formatValid( std::string filename );
        bool elementExists( std::string key );
        std::string trimLine( std::string line );
        bool isComment( std::string line );
        void parseLine( std::string line );
        void parseData( std::string filename );
        
    public:
        std::map<std::string, std::string> data;

        Config( std::string filename, char delimiter = '=', char commentDelimiter = '#' );

        std::string operator[] ( std::string key );
    };
} // namespace opcd

#endif // OPCD_CONFIG_HPP