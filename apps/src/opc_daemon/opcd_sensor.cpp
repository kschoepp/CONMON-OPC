#include "opcd_sensor.hpp"

namespace opcd
{
    Sensor::Sensor( int id, int machineId, unsigned int nodeNamespace, unsigned int nodeId, std::string dataType, std::string unit, double minDelta, std::string name, bool active, std::string creationTime, std::string dataTargets )
    {
        setID( id );
        setMachineID( machineId );
        setNamespace( nodeNamespace );
        setNodeID( nodeId );
        setDataType( dataType );
        setUnit( unit );
        setMinDelta( minDelta );
        setName( name );
        setActive( active );
        setCreationTime( creationTime );
        setDataTargets( dataTargets );
    }

    void Sensor::setID( int id )
    {
        this->id = id;
    }

    int Sensor::getID()
    {
        return id;
    }
    
    void Sensor::setMachineID( int machineId )
    {
        this->machineId = machineId;
    }

    int Sensor::getMachineID()
    {
        return machineId;
    }

    void Sensor::setNamespace( unsigned int nodeNamespace )
    {
        this->nodeNamespace = nodeNamespace;
    }

    unsigned int Sensor::getNamespace()
    {
        return nodeNamespace;
    }

    void Sensor::setNodeID( unsigned int nodeId )
    {
        this->nodeId = nodeId;
    }

    unsigned int Sensor::getNodeID()
    {
        return nodeId;
    }

    void Sensor::setDataType( std::string dataType )
    {
        this->dataType = dataType;
    }

    std::string Sensor::getDataType()
    {
        return dataType;
    }

    void Sensor::setUnit( std::string unit )
    {
        this->unit = unit;
    }

    std::string Sensor::getUnit()
    {
        return unit;
    }

    void Sensor::setMinDelta( double minDelta )
    {
        this->minDelta = minDelta;
    }

    double Sensor::getMinDelta()
    {
        return minDelta;
    }

    void Sensor::setName( std::string name )
    {
        this->name = name;
    }

    std::string Sensor::getName()
    {
        return name;
    }

    void Sensor::setActive( bool active )
    {
        this->active = active;
    }

    bool Sensor::isActive()
    {
        return active;
    }

    void Sensor::setCreationTime( std::string creationTime )
    {
        this->creationTime = creationTime;
    }

    std::string Sensor::getCreationTime()
    {
        return creationTime;
    }

    void Sensor::setDataTargets( std::string dataTargets )
    {
        this->dataTargets = dataTargets;
    }

    std::string Sensor::getDataTargets()    
    {
        return dataTargets;
    }
} // namespace opcd