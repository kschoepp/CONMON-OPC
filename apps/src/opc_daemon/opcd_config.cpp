#include "opcd_config.hpp"

namespace opcd
{
    Config::Config( std::string filename, char delimiter, char commentDelimiter )
    {
        setDelimiter( delimiter );
        setCommentDelimiter( commentDelimiter );

        if ( fileExists( filename ) )
        {
            parseData( filename );
        }
        else
        {
            throw OPCDConfigMissingException( filename );
        }
    }

    void Config::setDelimiter( char delimiter )
    {
        this->delimiter = delimiter;
    }

    char Config::getDelimiter()
    {
        return delimiter;
    }

    void Config::setCommentDelimiter( char commentDelimiter )
    {
        this->commentDelimiter = commentDelimiter;
    }

    char Config::getCommentDelimiter()
    {
        return commentDelimiter;
    }

    void Config::getStreamObject( std::string filename, std::ifstream & file )
    {
        file.open( filename, std::ifstream::out | std::ifstream::app );
    }

    bool Config::fileExists( std::string filename )
    {
        if ( std::ifstream( filename ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool Config::formatValid( std::string line )
    {
        line = trimLine( line );

        if ( std::count( line.begin(), line.end(), getDelimiter() ) == 1 && line.find( getDelimiter() ) != 0 && line.find( getDelimiter() ) != ( line.length() - 1 ) )
        {
            return true;
        }

        return false;
    }

    bool Config::Config::elementExists( std::string key )
    {
        // If the key already exists.
        if ( data.find( key ) != data.end() )
        {
            return true;
        }

        return false;
    }

    std::string Config::trimLine( std::string line )
    {
        size_t first = line.find_first_not_of(' ');

        if ( first == std::string::npos )
        {
            return "";
        }

        size_t last = line.find_last_not_of(' ');

        return line.substr( first, ( last - first + 1 ) );
    }

    bool Config::isComment( std::string line )
    {
        line = trimLine( line );

        if ( line[0] == getCommentDelimiter() )
        {
            return true;
        }

        return false;
    }

    void Config::parseLine( std::string line )
    {
        if ( isComment( line ) )
        {
            return;
        }

        if ( formatValid( line ) )
        {
            std::string key, value;
            std::istringstream issLine( line );

            // Parse the line until we fetch our delimiter, and trim it.
            std::getline( issLine, key, getDelimiter() );
            key = trimLine( key );

            // Parse the rest of the line and trim it.
            std::getline( issLine, value );
            value = trimLine( value );

            if ( value.back() == '\r' )
            {
                value.erase( value.size() -1 );
            }
        
            // Add Value
            if ( !elementExists( key ) )
            {
                data[key] = value;
            }
            else
            {
                throw OPCDConfigMalformedException( "\"" + key + "\" has multiple occurences" );
            }
        }
        else
        {
            throw OPCDConfigMalformedException( "Line: " + line );
        }
    }

    void Config::parseData( std::string filename )
    {
        std::string line;
        std::ifstream file;
        getStreamObject( filename, file );

        while ( std::getline( file, line ) )
        {
            parseLine( line );
        }
    }

    std::string Config::operator[] ( std::string key )
    {
        if ( !elementExists( key ) )
        {
            throw OPCDConfigKeyNotFoundException( __FILE__, __LINE__, key );
        }

        std::string value = "";
        std::map<std::string, std::string>::iterator pos = data.find( key );

        if ( pos != data.end() )
        {
            value = pos->second;
        }

        return value;
    }
} // namespace opcd