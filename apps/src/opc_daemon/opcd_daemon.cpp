#include "opcd_daemon.hpp"

namespace opcd
{
    Daemon::Daemon( std::string configFile )
    {
        setConfigFile( configFile );
        boot();
    }

    Daemon::~Daemon()
    {
        shutdown();
    }

    void Daemon::setStarted( bool started )
    {
        this->started = started;
    }

    bool Daemon::isStarted()
    {
        return started;
    }

    void Daemon::setConfigFile( std::string configFile )
    {
        this->configFile = configFile;
    }

    std::string Daemon::getConfigFile()
    {
        return configFile;
    }

    void Daemon::initConfig()
    {
        config = new Config( getConfigFile() );
    }

    Config * Daemon::configInstance()
    {
        return config;
    }

    void Daemon::deleteConfig()
    {
        delete config;
    }

    void Daemon::initLogger()
    {
        logger = new Logger( (*configInstance())["LOG_NAME"], (*configInstance())["LOG_PATH"] );
        logger->setActive(true);
        loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": ---BOOTING.." );
        loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": Logger has been successfully initialized." );
    }

    Logger * Daemon::loggerInstance()
    {
        return logger;
    }

    void Daemon::deleteLogger()
    {
        delete logger;
    }

    void Daemon::initDatabaseConnector()
    {
        databaseConnector = new DatabaseConnector( (*configInstance())["DB_HOST"], (*configInstance())["DB_PORT"], (*configInstance())["DB_NAME"], (*configInstance())["DB_USER"], (*configInstance())["DB_PASSWORD"] );
        databaseConnector->connect();

        loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": DatabaseConnector has been successfully initialized." );
    }

    DatabaseConnector * Daemon::databaseConnectorInstance()
    {
        return databaseConnector;
    }

    void Daemon::deleteDatabaseConnector()
    {
        delete databaseConnector;
    }

    void Daemon::initDataController()
    {
        dataController = new DataController( this );

        loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": DataController has been successfully initialized." );
    }

    DataController * Daemon::dataControllerInstance()
    {
        return dataController;
    }

    void Daemon::deleteDataController()
    {
        delete dataController;
    }

    void Daemon::initListener()
    {
        int clock = std::atoi( (*configInstance())["UPDATE_SUBSCRIPTION_CLOCK"].c_str() );
        listener = new Listener( this, clock );

        loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": Listener has been successfully initialized." );
    }

    Listener * Daemon::listenerInstance()
    {
        return listener;
    }

    void Daemon::deleteListener()
    {
        delete listener;
    }

    void Daemon::initModules()
    {
        initConfig();
        initLogger();
        loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": --Initializing all Modules..." );
        initDatabaseConnector();
        initDataController();
        initListener();
        
        loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": --Modules have been successfully initialized." );
    }
    
    void Daemon::deleteModules()
    {
        loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": --Deactivating all Modules..." );
        
        deleteListener();
        deleteDataController();
        deleteDatabaseConnector();
        deleteConfig();
        
        loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": --Modules have been successfully initialized." );
        
        deleteLogger();
    }
    
    void Daemon::boot()
    {
        if ( !isStarted() )
        {
            initModules();
            dataControllerInstance()->addData();
            listenerInstance()->addClients();
            setStarted( true );

            loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": ---BOOTING DONE" );
            //std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
            startUpdateLoop();
        }
        else
        {
            loggerInstance()->writeWarn( std::string( "" ) + typeid(*this).name() + ": ---DAEMON ALREADY BOOTED UP." );
        }
    }

    void Daemon::shutdown()
    {
        if ( isStarted() )
        {
            loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": ---SHUTTING DOWN" );
    
            stopUpdateLoop();
            setStarted( false );
    
            listenerInstance()->deleteClients();
            dataControllerInstance()->deleteData();
            deleteModules();
    
            std::cout << "Daemon shut down successfully." << std::endl;
        }
        else
        {
            loggerInstance()->writeWarn( std::string( "" ) + typeid(*this).name() + ": ---DAEMON NOT STARTED, CAN'T SHUTDOWN." );
        }
    }

    void Daemon::restart()
    {
        loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": ---RESTARTING" );
        shutdown();
        boot();
        loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": ---RESTARTING DONE" );
    }

    void Daemon::setUpdateLoopActive( bool active )
    {
        this->updateLoopActive = active;
    }

    bool Daemon::isUpdateLoopActive()
    {
        return updateLoopActive;
    }

    void Daemon::startUpdateLoop()
    {
        setUpdateLoopActive( true );
        std::thread( &Daemon::updateLoop_Static, this ).detach();
    }

    void Daemon::stopUpdateLoop()
    {
        setUpdateLoopActive( false );
    }

    void Daemon::updateLoop()
    {
        while ( isUpdateLoopActive() && isStarted() )
		{
            loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": --Updating all data." );
            dataControllerInstance()->updateData();
            listenerInstance()->updateClients();
            loggerInstance()->writeInfo( std::string( "" ) + typeid(*this).name() + ": --Done updating." );
            
            int clock = std::atoi( (*configInstance())["UPDATE_CLOCK"].c_str() );
			std::this_thread::sleep_for( std::chrono::seconds( clock ) );
		}
    }

    void Daemon::updateLoop_Static( Daemon * daemon )
    {
        daemon->updateLoop();
    }
} // namespace opcd