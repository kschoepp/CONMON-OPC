**OPC UA Apps and Docker Enviroment**
======

## **Docker**
### **Image**

The Docker Image "opcua" contains all neccessary tools and libraries to build and run applications using FreeOpcUa and PostgreSQL. It can be used for compiling-purposes or as runtime-enviroment for our applications.

* The Dockerfile can either be build manually, or via our Makefile. 
 * FreeOpcUa and LibPQXX will be cloned automatically into the same directory as our Dockerfile.
 * All data (including the compiled libraries) from FreeOpcUa is stored in /freeopcua.

``` sh
$ cd ./docker/opcua
$ make
```

### **Compose file**

* A reference compose file is included.
* The services are:
 * devenv
 * compiler
 * daemon
 * server
 * client
 * demo_server
 * demo_server1
 * demo_server2
 * demo_client
* Network: opcua-network
* Environment-File: .env

#### **Reverse Proxies**

* FreeOpcUa has been successfully tested with various TCP Reverse Proxies, and should work with reverse proxies such as the pendants from nginx.
* If you want to use the rProxy with a specific subdomain, you have to expose the specific Port of the used Application on the container nevertheless. Because you don't want to make that port public in this case, it is recommended to use the opcua-network, and only communicate via the specific IP-Address/Hostnames of that container.

## **Applications**

* Our applications are stored in ./apps.
* Apps included:
 * **opc_daemon**
 * opc_server
 * opc_client
 * opc_demo_server
 * opc_demo_client
* opc_daemon needs a valid config-file as argument
* The other Apps need "host:port" as argument

``` sh
$ cd ./docker/opcua
$ docker-compose run --rm demo_server1
$ docker-compose run --rm demo_server2
--OR--
$ /apps/bin/opc_demo_server/opc_demo_server.exe localhost:4840

$ docker-compose run --rm daemon
--OR--
$ cd /apps/bin/opc_daemon; ./opc_daemon.exe config.cfg 
```

* All Apps can be build with a Makefile.

``` sh
$ cd ./docker/opcua
$ docker-compose run --rm compiler
--OR--
$ cd ./docker/opcua
$ docker-compose run --rm devenv -c "cd /apps; make"
--OR--
$ docker run -it --rm -v ./apps:/apps opcua /bin/bash -c "cd /apps; make"
```