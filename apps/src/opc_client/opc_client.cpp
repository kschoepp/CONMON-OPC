/// @author Alexander Rykovanov 2013
/// @email rykovanov.as@gmail.com
/// @brief Remote Computer implementaion.
/// @license GNU LGPL
///
/// Distributed under the GNU LGPL License
/// (See accompanying file LICENSE or copy at
/// http://www.gnu.org/licenses/lgpl.html)
///

#include <opc/ua/client/client.h>
#include <opc/ua/node.h>
#include <opc/ua/subscription.h>

#include <iostream>
#include <stdexcept>
#include <thread>

using namespace OpcUa;

class SubClient : public SubscriptionHandler
{
  void DataChange( uint32_t handle, const Node& node, const Variant& val, AttributeId attr ) override
  {
    std::cout << "-----Received DataChange event, value of Node " << node << " is now: "  << val.ToString() << std::endl;
  }
};

int main( int argc, char* argv[] )
{
  try
  {
	std::string endpoint = argv[1];
    endpoint = "opc.tcp://" + endpoint + "/";

    std::cout << "-----Connecting to: " << endpoint << std::endl;
    bool debug = false;
    OpcUa::UaClient client( debug );
    client.Connect( endpoint );

    OpcUa::Node root = client.GetRootNode();
    std::cout << "-----Root node is: " << root << std::endl;

    std::cout << "-----Child of objects node are: " << std::endl;
    Node objects = client.GetObjectsNode();

	for ( OpcUa::Node node : objects.GetChildren() )
	{
		std::cout << "    " << node << std::endl;
	}

    std::cout << "-----NamespaceArray is: " << std::endl;
    OpcUa::Node nsnode = client.GetNode( ObjectId::Server_NamespaceArray );
    OpcUa::Variant ns = nsnode.GetValue();

    for ( std::string d : ns.As<std::vector<std::string>>() )
	{
      std::cout << "    " << d << std::endl;
	}

    OpcUa::Node myvar;
    OpcUa::Node myobject;
    OpcUa::Node mymethod;

    uint32_t idx = client.GetNamespaceIndex( "demo_server" );
    std::vector<std::string> objpath( { std::to_string(idx) + ":NewObject" } );
    myobject = objects.GetChild( objpath );

    std::vector<std::string> methodpath( { objpath.front(), "GetTime" } );
    mymethod = objects.GetChild( methodpath );

    std::vector<OpcUa::Variant> arguments;
    arguments.push_back( static_cast<uint8_t>(0) );
	
	std::string val = myobject.CallMethod( mymethod.GetId(), arguments ).front().ToString();
    std::cout << val << std::endl;

    // Example from any UA server, standard dynamic variable node:
    std::vector<std::string> varpath{ std::to_string(idx) + ":NewObject", "counterVar" };
    myvar = objects.GetChild( varpath );

    std::cout << "-----got node: " << myvar << std::endl;

	std::cout << "Ctrl-C to exit" << std::endl;
	for (;;)
	{
		std::cout << myvar.GetValue().ToString() << std::endl;
		std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
	}

    std::cout << "-----Disconnecting" << std::endl;
    client.Disconnect();


    return 0;
  }
  catch (const std::exception& exc)
  {
    std::cout << exc.what() << std::endl;
  }
  catch (...)
  {
    std::cout << "-----Unknown error." << std::endl;
  }
  return -1;
}

