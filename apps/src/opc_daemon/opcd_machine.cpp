#include "opcd_machine.hpp"

namespace opcd
{
    Machine::Machine( int id, std::string uri, std::string name, bool active, std::string creationTime, std::string location, std::string description )
    {
        setID( id );
        setURI( uri );
        setName( name );
        setActive( active );
        setCreationTime( creationTime );
        setLocation( location );
        setDescription( description );
        setStatus( OpcUa::StatusCode::Good );
        setNumSensors( 0 );
    }
    void Machine::setID( int id )
    {
        this->id = id;
    }

    int Machine::getID()
    {
        return id;
    }
    void Machine::setURI( std::string uri )
    {
        if ( uri.empty() )
        {
            throw OPCDEmptyFieldException( "URI must not be empty." );
        }

        this->uri = uri;
    }

    std::string Machine::getURI()
    {
        return uri;
    }

    void Machine::setName( std::string name )
    {
        this->name = name;
    }

    std::string Machine::getName()
    {
        return name;
    }

    void Machine::setActive( bool active )
    {
        this->active = active;
    }
    bool Machine::isActive()
    {
        return active;
    }

    void Machine::setCreationTime( std::string creationTime )
    {
        this->creationTime = creationTime;
    }

    std::string Machine::getCreationTime()
    {
        return creationTime;
    }

    void Machine::setLocation( std::string location )
    {
        this->location = location;
    }

    std::string Machine::getLocation()
    {
        return location;
    }

    void Machine::setDescription( std::string description )
    {
        this->description = description;
    }

    std::string Machine::getDescription()
    {
        return description;
    }

    void Machine::setStatus( OpcUa::StatusCode status )
    {
        this->status = status;
    }

    OpcUa::StatusCode Machine::getStatus()
    {
        return status;
    }

    bool Machine::isStatusGood()
	{
        OpcUa::StatusCode status = getStatus();

		return ( 	status == OpcUa::StatusCode::Good || status == OpcUa::StatusCode::GoodSubscriptionTransferred || status == OpcUa::StatusCode::GoodCompletesAsynchronously ||
					status == OpcUa::StatusCode::GoodOverload || status == OpcUa::StatusCode::GoodClamped || status == OpcUa::StatusCode::GoodResultsMayBeIncomplete ||
					status == OpcUa::StatusCode::GoodLocalOverride || status == OpcUa::StatusCode::GoodEntryInserted || status == OpcUa::StatusCode::GoodEntryReplaced ||
					status == OpcUa::StatusCode::GoodNoData || status == OpcUa::StatusCode::GoodMoreData || status == OpcUa::StatusCode::GoodDataIgnored ||
					status == OpcUa::StatusCode::GoodCommunicationEvent || status == OpcUa::StatusCode::GoodShutdownEvent || status == OpcUa::StatusCode::GoodCallAgain ||
					status == OpcUa::StatusCode::GoodNonCriticalTimeout );
	}

    void Machine::setNumSensors( unsigned int numSensor )
    {
        this->numSensors = numSensors;
    }

    unsigned int Machine::getNumSensors()
    {
        return numSensors;
    }
} // namespace opcd