#include <iostream>
#include <exception>

#include <pqxx/pqxx>

#include "opcd_database_connector.hpp"

int main()
{
    try
    {
        opcd::DatabaseConnector dbConnector( "ingence.de", "25432", "conmon", "postgres", "psql" );
            dbConnector.connect();
            dbConnector.query(
                "INSERT INTO sensorData (sid, value, ts)"
                "VALUES (" 
                + std::to_string( 69 ) + ", "
                + "'" + "lol" + "'" + ", "
                + "NOW()"
                ");"
            );
        
        pqxx::result r = dbConnector.query( "SELECT * FROM sensordata;" );
        
        for ( int rowNum = 0; rowNum < r.size(); ++rowNum )
        {
            const pqxx::row row = r[rowNum];

            for ( int colNum = 0; colNum < row.size(); ++colNum )
            {
                const pqxx::field cell = row[colNum];
                std::cout << cell.c_str() << '\t';
            }

            std::cout << std::endl;
        }
    }
    catch( pqxx::sql_error &e )
    {
        std::cerr << "Exception: " << "SQL error: " << e.what() << std::endl;
        std::cerr << "Exception: " << "Query was: " << e.query() << std::endl;
        return 2;
    }
    catch( opcd::DatabaseNotConnectedException &e )
    {
        std::cerr << "Exception: " << e.what() << std::endl;
        
        return 1;
    }
    catch( std::exception &e )
    {
        std::cerr << "Exception: " << e.what() << std::endl;
        
        return 1;
    }
    
    return 0;
}
