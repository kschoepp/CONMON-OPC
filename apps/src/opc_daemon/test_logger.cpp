#include "opcd_logger.hpp"

int main()
{
    opcd::Logger log( "test", "./logs" );

    log.writeInfo( "testinfo" );
    log.writeWarn( "testwarn" );
    log.writeDebug( "testdebug" );
    log.writeError( "testerror" );

    return 0;
}
