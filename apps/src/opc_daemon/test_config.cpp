#include <iostream>
#include <exception>

#include "opcd_config.hpp"
#include "opcd_exceptions.hpp"

int main()
{
    try
    {
        opcd::Config config( "./tests/test.cfg" );
        //config["lulz"] = "asdf";
        //std::cout << "bla: " << config["bla"] << std::endl;

        for ( std::map<std::string, std::string>::iterator it = config.data.begin(); it != config.data.end(); ++it )
        {
            std::cout << it->first << " == " << it->second << std::endl;
        }
    }
    catch ( opcd::ConfigMalformedException & e )
    {
        std::cerr << "ConfigMalformedException: " << e.what() << std::endl;
    }
    catch ( opcd::ConfigMissingException & e )
    {
        std::cerr << "ConfigMissingException: " << e.what() << std::endl;
    }
    catch ( opcd::ConfigKeyNotFoundException & e )
    {
        std::cerr << "ConfigKeyNotFoundException: " << e.what() << std::endl;
    }
    catch ( opcd::Exception & e )
    {
        std::cerr << "Exception: " << e.what() << std::endl;
    }

    return 0;
}
